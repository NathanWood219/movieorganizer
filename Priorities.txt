NEEDED TO FINISH PROJECT
	[DONE] Change movies to store absolute filepath
	[DONE] Manually searching for correct data for movies
	[DONE] Add option when scanning to search full depth of movies folder
	[DONE] Checking for updates

NICE TO HAVE
	[DONE] Bug reporting
	Sorting number columns by value, not alphabetically
	Movie resolution

EXTRA FEATURES
	Exporting searches as a text file list
	Browse movies by poster