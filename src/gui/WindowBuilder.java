package gui;

import application.MainApplication;
import controllers.WorkspaceController;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.geometry.Pos;
import static css.StyleConstants.*;
import data.Movie;
import database.SearchQuery;
import database.SearchResult;
import java.awt.Desktop;
import java.io.File;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Priority;
import org.controlsfx.control.textfield.TextFields;
import strings.StringParser;
import tasks.ReportTask;
import tasks.ScanTask;

/**
 *  The <code>WindowBuilder</code> class creates custom pop-up windows and dialogs.
 * 
 * @author Nathan
 */
public class WindowBuilder {
    
    // METHODS
    public static void buildScanPromptWindow(MainApplication app) {
        app.trace("Building scan prompt window.");
        
        Label directoryLabel = new Label(" " + app.getSettingsComponent().getMoviesPath());
        directoryLabel.setStyle(CSS_PANE_BORDER + "-fx-border-color: gray; ");
        directoryLabel.setMinWidth(260);
        directoryLabel.setMaxWidth(260);
        directoryLabel.setMinHeight(25);
        
        Button changeDirectoryButton = new Button("Change");
        
        CheckBox scanTypeCheckBox = new CheckBox("Scan inside nested folders");
        scanTypeCheckBox.setSelected(app.getSettingsComponent().getScanNested());
        
        Button scanButton = new Button("Start Scan");
        Button cancelButton = new Button("Cancel");
        
        VBox leftVBox = new VBox(directoryLabel, scanTypeCheckBox, scanButton);
        leftVBox.setSpacing(10);
        leftVBox.setAlignment(Pos.TOP_LEFT);
        
        VBox rightVBox = new VBox(changeDirectoryButton, new Label(""), cancelButton);
        rightVBox.setMinWidth(70);
        rightVBox.setSpacing(10);
        rightVBox.setAlignment(Pos.TOP_RIGHT);
        
        // BUILD MAIN CONTAINER AND SCENE
        HBox mainHBox = new HBox(leftVBox, rightVBox);
        mainHBox.setPadding(new Insets(10));
        
        Scene child = new Scene(mainHBox, 340, 100);
        child.getStylesheets().add("css/style.css");
        mainHBox.getStyleClass().add(WINDOW);
        
        // BUILD THE STAGE
        Stage stage = buildChildStage(app, child);
        stage.setTitle("Scan Movies");
        
        // SET HANDLERS
        WorkspaceController workCont = new WorkspaceController(app);
        
        changeDirectoryButton.setOnAction(e -> { workCont.processChangeDirectory();
                    directoryLabel.setText(" " + app.getSettingsComponent().getMoviesPath()); });
        cancelButton.setOnAction(e -> { stage.close(); app.trace("Cancelling scan."); });
        scanButton.setOnAction(e -> { workCont.processScanMovies(scanTypeCheckBox.isSelected()); stage.close(); });
        
        // SET LISTENERS
        scanTypeCheckBox.selectedProperty().addListener(e -> {
            app.getSettingsComponent().setScanNested(scanTypeCheckBox.isSelected());
        });
    }
    
    public static void buildScanReportWindow(MainApplication app, ScanTask scanTask) {
        app.trace("Scan report window created.");
        
        Label finishedLabel = new Label("The scan completed with " + scanTask.getNewMovieCount() + " new movies and " + scanTask.getErrorCount() + " errors.");
        finishedLabel.setWrapText(true);
        
        // CREATE CONTROLS
        Button okayButton = new Button("Close");
        
        // PLACE CONTROLS IN BOXES THAT ALWAYS STAY TO THE LEFT AND RIGHT OF THE STAGE 
        HBox controlsHBox = new HBox(okayButton);
        controlsHBox.setAlignment(Pos.CENTER);
        
        VBox topVBox = new VBox(finishedLabel);
        topVBox.setAlignment(Pos.TOP_LEFT);
        VBox.setVgrow(topVBox, Priority.ALWAYS);
        
        VBox bottomVBox = new VBox(controlsHBox);
        bottomVBox.setAlignment(Pos.BOTTOM_LEFT);
        VBox.setVgrow(bottomVBox, Priority.ALWAYS);
        
        VBox mainVBox = new VBox(topVBox, bottomVBox);
        mainVBox.setPadding(new Insets(10));
        
        Scene child = new Scene(mainVBox, 320, 70);
        child.getStylesheets().add("css/style.css");
        mainVBox.getStyleClass().add(WINDOW);
        
        Stage stage = buildChildStage(app, child);
        stage.setTitle("Scan Complete");
        
        // SET HANDLERS
        okayButton.setOnAction(e -> {
            app.trace("Closing scan report.");
            stage.close();
        });
    }
    
    public static void buildCorrectMovieWindow(MainApplication app, Movie movie, boolean isError) {
        app.trace("Building correct movie window for '" + movie.getFilename() + "'");
        
        // TOP LABELS
        Label correctMovieLabel = new Label("\"" + movie.getFilename() + "\"");
        correctMovieLabel.setStyle(CSS_BOLD + CSS_FONT_SIZE + "16;");
        
        HBox correctMovieHBox = new HBox(correctMovieLabel);
        correctMovieHBox.setAlignment(Pos.CENTER);
        
        // SEARCH COMPONENTS FOR MOVIES: TITLE, YEAR
        Label titleLabel = new Label("Title:");
        Label yearLabel = new Label("Year:");
        
        TextField titleTextField = TextFields.createClearableTextField();
        titleTextField.setText(movie.getTitle());
        titleTextField.setPrefWidth(181);
        
        TextField yearTextField = TextFields.createClearableTextField();
        yearTextField.setText(movie.getYear());
        yearTextField.setPrefWidth(181);
        
        // SEARCH BUTTON MAN
        Button searchButton = new Button("Search");
        
        searchButton.setDisable(titleTextField.getText().isEmpty() || !StringParser.isDigits(yearTextField.getText()));
        
        // SEARCH HBOX CONTAINER
        HBox searchHBox = new HBox(titleLabel, titleTextField,
                yearLabel, yearTextField, searchButton);
        searchHBox.setAlignment(Pos.CENTER);
        searchHBox.setSpacing(6);
        
        SearchQuery query = new SearchQuery();
        
        // SEARCHQUERY TABLEVIEW
        TableView<SearchResult> searchTableView = CorrectMovieWindow.buildSearchTable(query.getResults());
        
        // BOTTOM CONTROLS
        
        Button correctButton = new Button("Correct");
        correctButton.setDisable(true);
        
        Button cancelButton = new Button("Cancel");
        
        HBox leftControlHBox = new HBox(correctButton);
        leftControlHBox.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(leftControlHBox, Priority.ALWAYS);
        
        HBox rightControlHBox = new HBox(cancelButton);
        rightControlHBox.setAlignment(Pos.CENTER_RIGHT);
        HBox.setHgrow(rightControlHBox, Priority.ALWAYS);
        
        HBox controlHBox = new HBox(leftControlHBox, rightControlHBox);
        
        // BUILD MAIN CONTAINER AND SCENE
        VBox mainVBox = new VBox(correctMovieHBox, searchHBox,
                searchTableView, controlHBox);
        mainVBox.setSpacing(10);
        mainVBox.setPadding(new Insets(10));
        
        Scene child = new Scene(mainVBox, 500, 400);
        child.getStylesheets().add("css/style.css");
        
        // BUILD THE STAGE
        Stage stage = buildChildStage(app, child);
        stage.setTitle("Correct Movie Data");
        
        // ATTACH TABLEVIEW HEIGHT TO STAGE HEIGHT
        searchTableView.prefHeightProperty().bind(stage.heightProperty());
        
        // SET HANDLERS
        titleTextField.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER && !searchButton.isDisabled()) {
                CorrectMovieWindow.processSearchDatabase(titleTextField.getText(), yearTextField.getText(), query, app);
            }
        });
        
        yearTextField.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER && !searchButton.isDisabled()) {
                CorrectMovieWindow.processSearchDatabase(titleTextField.getText(), yearTextField.getText(), query, app);
            }
        });
        
        searchButton.setOnAction(e -> {
            CorrectMovieWindow.processSearchDatabase(titleTextField.getText(), yearTextField.getText(), query, app);
        });
        
        cancelButton.setOnAction(e -> {
            stage.close();
            app.trace("Correct movie process cancelled.");
        });
        
        correctButton.setOnAction(e -> {
            SearchResult result = searchTableView.getSelectionModel().getSelectedItem();
            
            if(result != null) {
                CorrectMovieWindow.processCorrectData(movie, result, isError, app);
            }
            
            stage.close();
        });
        
        // SET LISTENERS
        searchTableView.getSelectionModel().selectedItemProperty().addListener(e -> {
            if(searchTableView.getSelectionModel().getSelectedItem() == null) {
                correctButton.setDisable(true);
            } else {
                correctButton.setDisable(false);
            }
        });
        
        titleTextField.textProperty().addListener(e -> {
            searchButton.setDisable(titleTextField.getText().isEmpty() || !StringParser.isDigits(yearTextField.getText()));
        });
        
        yearTextField.textProperty().addListener(e -> {
            searchButton.setDisable(titleTextField.getText().isEmpty() || !StringParser.isDigits(yearTextField.getText()));
        });
        
        // PERFORM INITIAL SEARCH IF POSSIBLE
        if(!searchButton.isDisabled()) {
            CorrectMovieWindow.processSearchDatabase(titleTextField.getText(), yearTextField.getText(), query, app);
        }
    }
    
    public static void buildBugReportWindow(MainApplication app) {
        app.trace("Building bug report window.");
        
        Label bugLabel = new Label("Please describe the events leading up to this bug:");
        
        TextArea bugTextArea = new TextArea();
        bugTextArea.setPrefHeight(100);
        
        CheckBox systemCheckBox = new CheckBox("Include system information in report");
        
        // BOTTOM CONTROLS
        Button confirmButton = new Button("Send Bug Report");
        Button cancelButton = new Button("Don't Send");
        
        HBox leftControlHBox = new HBox(confirmButton);
        leftControlHBox.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(leftControlHBox, Priority.ALWAYS);
        
        HBox rightControlHBox = new HBox(cancelButton);
        rightControlHBox.setAlignment(Pos.CENTER_RIGHT);
        HBox.setHgrow(rightControlHBox, Priority.ALWAYS);
        
        HBox controlHBox = new HBox(leftControlHBox, rightControlHBox);
        
        // BUILD MAIN CONTAINER AND SCENE
        VBox mainVBox = new VBox(bugLabel, bugTextArea, systemCheckBox, controlHBox);
        mainVBox.setSpacing(10);
        mainVBox.setPadding(new Insets(10));
        
        Scene child = new Scene(mainVBox, 300, 200);
        child.getStylesheets().add("css/style.css");
        mainVBox.getStyleClass().add(WINDOW);
        
        // BUILD THE STAGE
        Stage stage = buildChildStage(app, child);
        stage.setTitle("Report a Bug");
        
        // SET HANDLERS
        confirmButton.setOnAction(e -> {
            
            // CREATE REPORT TASK
            ReportTask reportTask = new ReportTask(app,
                    bugTextArea.getText(), systemCheckBox.isSelected());
            
            new Thread(reportTask).start();
            
            stage.close();
        });
        
        cancelButton.setOnAction(e -> {
            stage.close();
            app.trace("Bug reporting cancelled.");
        });
    }
    
    public static void buildBugWindow(MainApplication app, String errorString) {
        app.trace("Error window created: " + errorString);
        
        Label errorLabel = new Label(errorString);
        errorLabel.setWrapText(true);
        
        // CREATE CONTROLS
        Button reportButton = new Button("Report Bug");
        Button cancelButton = new Button("Cancel");
        
        // PLACE CONTROLS IN BOXES THAT ALWAYS STAY TO THE LEFT AND RIGHT OF THE STAGE
        HBox leftControlHBox = new HBox(reportButton);
        leftControlHBox.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(leftControlHBox, Priority.ALWAYS);
        
        HBox rightControlHBox = new HBox(cancelButton);
        rightControlHBox.setAlignment(Pos.CENTER_RIGHT);
        HBox.setHgrow(rightControlHBox, Priority.ALWAYS);
        
        HBox controlsHBox = new HBox(leftControlHBox, rightControlHBox);
        
        VBox topVBox = new VBox(errorLabel);
        topVBox.setAlignment(Pos.TOP_LEFT);
        VBox.setVgrow(topVBox, Priority.ALWAYS);
        
        VBox bottomVBox = new VBox(controlsHBox);
        bottomVBox.setAlignment(Pos.BOTTOM_LEFT);
        VBox.setVgrow(bottomVBox, Priority.ALWAYS);
        
        VBox mainVBox = new VBox(topVBox, bottomVBox);
        mainVBox.setPadding(new Insets(10));
        
        Scene child = new Scene(mainVBox, 320, 60);
        child.getStylesheets().add("css/style.css");
        mainVBox.getStyleClass().add(WINDOW);
        
        Stage stage = buildChildStage(app, child);
        stage.setTitle("Error");
        
        // SET HANDLERS
        reportButton.setOnAction(e -> {
            app.trace("Opening bug report window.");
            stage.close();
            buildBugReportWindow(app);
        });
        
        cancelButton.setOnAction(e -> {
            app.trace("User chose not to report bug.");
            stage.close();
        });
    }
    
    public static void buildHelpWindow(MainApplication app) {
        
    }
    
    public static void buildUpdatesWindow(MainApplication app) {
        app.trace("Building updates window.");
        
        Label updateLabel = new Label("An update is available. Would you like to download it?");
        
        // CREATE CONTROLS
        Button updateButton = new Button("Update");
        Button cancelButton = new Button("Cancel");
        
        // PLACE CONTROLS IN BOXES THAT ALWAYS STAY TO THE LEFT AND RIGHT OF THE STAGE
        HBox leftControlHBox = new HBox(updateButton);
        leftControlHBox.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(leftControlHBox, Priority.ALWAYS);
        
        HBox rightControlHBox = new HBox(cancelButton);
        rightControlHBox.setAlignment(Pos.CENTER_RIGHT);
        HBox.setHgrow(rightControlHBox, Priority.ALWAYS);
        
        HBox controlsHBox = new HBox(leftControlHBox, rightControlHBox);
        
        VBox mainVBox = new VBox(updateLabel, controlsHBox);
        mainVBox.setPadding(new Insets(5));
        mainVBox.setSpacing(12);
        
        Scene child = new Scene(mainVBox, 300, 60);
        child.getStylesheets().add("css/style.css");
        mainVBox.getStyleClass().add(WINDOW);
        
        // FINALLY, BUILD THE STAGE
        Stage stage = buildChildStage(app, child);
        stage.setTitle("Updates");
        
        // SET HANDLERS
        updateButton.setOnAction(e -> {
            
            try {
                // OPEN UPDATER
                app.trace("Opening updater...");
                Desktop.getDesktop().open(new File("./lib/updater.jar"));
                app.trace("Sucessfully opened updater.");
                
                // EXIT APPLICATION
                WorkspaceController workCont = new WorkspaceController(app);
                workCont.processExit();
                
            } catch(Exception ex) {
                app.traceEx(ex);
            }
        });
        
        cancelButton.setOnAction(e -> {
            app.trace("Update was cancelled.");
            stage.close();
        });
    }
    
    public static void buildAboutWindow(MainApplication app) {
        app.trace("Building about window.");
        
        Label authorLabel = new Label("Author: Nathan Wood");
        
        /*
            AUTHOR
            TWITTER
            WEBSITE
            PORTFOLIO ?
        */
        
        VBox mainVBox = new VBox(authorLabel);
        
        Scene child = new Scene(mainVBox, 300, 300);
        child.getStylesheets().add("css/style.css");
        mainVBox.getStyleClass().add(WINDOW);
        
        // FINALLY, BUILD THE STAGE
        buildChildStage(app, child);
    }
    
    public static void buildOkWindow(MainApplication app, String title, String text) {
        Label finishedLabel = new Label(text);
        
        // CREATE CONTROLS
        Button okButton = new Button("Okay");
        
        // PLACE CONTROLS IN BOXES THAT ALWAYS STAY TO THE LEFT AND RIGHT OF THE STAGE
        HBox controlsHBox = new HBox(okButton);
        controlsHBox.setAlignment(Pos.CENTER);
        HBox.setHgrow(controlsHBox, Priority.ALWAYS);
        
        VBox mainVBox = new VBox(finishedLabel, controlsHBox);
        mainVBox.setPadding(new Insets(5));
        mainVBox.setSpacing(12);
        
        Scene child = new Scene(mainVBox, 200, 60);
        child.getStylesheets().add("css/style.css");
        mainVBox.getStyleClass().add(WINDOW);
        
        Stage stage = new Stage();
        stage.setTitle(title);
        
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(app.scene().getWindow());
        stage.setScene(child);
        stage.setResizable(false);
        stage.show();
        
        // SET HANDLERS
        okButton.setOnAction(e -> {
            stage.close();
        });
    }
    
    // PRIVATE HELPERS
    private static Stage buildChildStage(MainApplication app, Scene child) {
        Stage stage = new Stage();
        
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(app.scene().getWindow());
        stage.setScene(child);
        stage.setResizable(false);
        stage.show();
        
        // PLACE WINDOW AT CENTER OF PARENT WINDOW
        stage.setX(app.stage().getX() + app.stage().getWidth() / 2 - stage.getWidth() / 2);
        stage.setY(app.stage().getY() + app.stage().getHeight() / 2 - stage.getHeight() / 2);
        
        return stage;
    }
}
