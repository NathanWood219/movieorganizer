package gui;

import application.MainApplication;
import static css.StyleConstants.CSS_ALIGN_CENTER;
import static css.StyleConstants.CSS_ALIGN_LEFT;
import static css.StyleConstants.CSS_ALIGN_RIGHT;
import data.Movie;
import database.SearchQuery;
import database.SearchResult;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import tasks.CorrectMovieTask;
import tasks.SearchMovieTask;

/**
 *
 * @author Nathan
 */
public class CorrectMovieWindow {
    
    
    public static void processSearchDatabase(String title, String year, SearchQuery query, MainApplication app) {
        app.trace("Performing database search query. Title = " + title + "; Year = " + year);
        
        try {
            SearchMovieTask searchTask = new SearchMovieTask(app, title, year);
            new Thread(searchTask).start();

            searchTask.setOnFailed(e -> {
                app.trace("Search task failed to complete.");
            });
            
            searchTask.setOnSucceeded(e -> {
                // LOAD DATA INTO SEARCH QUERY
                query.fromJSON(app.getFileComponent().loadJSONString(searchTask.getResponse()));
                app.trace("Finished loading search results.");
            });
            
            
        } catch(Exception ex) {
            app.trace("Failed to search OMDb API for '" + title + "'");
            app.traceEx(ex);
        }
    }
    
    public static void processCorrectData(Movie movie, SearchResult result, boolean isError, MainApplication app) {
        try {
            
            Movie oldMovie = movie.clone();
            
            CorrectMovieTask correctTask = new CorrectMovieTask(app, movie, result);
            new Thread(correctTask).start();
            
            correctTask.setOnFailed(e -> {
                app.trace("Correct movie task failed to complete.");
            });
            
            correctTask.setOnSucceeded(e -> {
            
                app.getWorkspaceComponent().removeTabFromMovie(oldMovie);
                
                if(isError) {
                    app.getDataComponent().addMovie(movie);
                    app.getDataComponent().removeError(movie);
                }
                
                app.getWorkspaceComponent().addTabFromMovie(movie);
                app.getWorkspaceComponent().refreshAndSaveMovies();
                
                app.trace("Finished correcting movie data.");
            });

        } catch(Exception ex) {
            app.trace("Failed to search OMDb API for ID: '" + result.getIMDBid() + "'");
            app.traceEx(ex);
        }
    }
    
    public static TableView<SearchResult> buildSearchTable(ObservableList<SearchResult> searchResults) {
        TableView<SearchResult> searchTable = new TableView<>();
        
        TableColumn<SearchResult, String>
            numberCol 		= new TableColumn<>("#"),
            titleCol 		= new TableColumn<>("Title"),
            yearCol 		= new TableColumn<>("Year"),
            imdbIDCol 		= new TableColumn<>("IMDB ID");
        
        numberCol.setCellValueFactory(new PropertyValueFactory<>("index"));
        titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        yearCol.setCellValueFactory(new PropertyValueFactory<>("year"));
        imdbIDCol.setCellValueFactory(new PropertyValueFactory<>("IMDBid"));
        
        // APPLY WIDTH AND CSS FORMATTING TO EACH COLUMN

        numberCol.setPrefWidth(35);
        numberCol.setResizable(false);
        numberCol.setStyle(CSS_ALIGN_RIGHT);

        titleCol.setMinWidth(313);
        titleCol.setStyle(CSS_ALIGN_LEFT);

        yearCol.setPrefWidth(60);
        yearCol.setResizable(false);
        yearCol.setStyle(CSS_ALIGN_CENTER);

        imdbIDCol.setPrefWidth(80);
        imdbIDCol.setResizable(false);
        imdbIDCol.setStyle(CSS_ALIGN_LEFT);

        // BUILD THE TABLEVIEW THAT WILL BE RETURNED
        searchTable.getColumns().addAll(numberCol, titleCol, yearCol, imdbIDCol);
        
        searchTable.setItems(searchResults);
        searchTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        // ADD LISTENER FOR SELECTION CHANGE
        searchTable.getSelectionModel().selectedItemProperty().addListener(e -> {
            SearchResult result = searchTable.getSelectionModel().getSelectedItem();
                
            if(result != null) {
                searchTable.setContextMenu(searchResultContextMenu(result));
            }
        });
        
        return searchTable;
    }
    
    public static ContextMenu searchResultContextMenu(SearchResult clickedResult) {
        
        ContextMenu context = new ContextMenu();
        
        MenuItem openIMDBItem;
        
        openIMDBItem = new MenuItem("Open in browser");
        
        openIMDBItem.setOnAction(e -> {
            MainApplication.processOpenLink("http://www.imdb.com/title/" + clickedResult.getIMDBid());
            context.hide();
        });
        
        context.getItems().addAll(openIMDBItem);
        
        return context;
    }
}
