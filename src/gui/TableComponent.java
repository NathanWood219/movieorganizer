package gui;

import application.MainApplication;
import controllers.TabPaneController;
import controllers.WorkspaceController;
import data.Movie;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import static css.StyleConstants.*;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

/**
 *
 * @author Nathan
 */
public class TableComponent {
    // ATTRIBUTES
    MainApplication app;
    
    Menu searchViewMenu;
    
    CheckMenuItem numberCheckItem, titleCheckItem, yearCheckItem,
        ratingCheckItem, runtimeCheckItem, genresCheckItem, directorsCheckItem,
        writersCheckItem, actorsCheckItem, languagesCheckItem,
        countriesCheckItem, metascoreCheckItem, imdbRatingCheckItem,
        imdbVotesCheckItem, filenameCheckItem;
    
    // CONSTRUCTORS
    public TableComponent(MainApplication app) {
        this.app = app;
        
        buildViewItems();
    }
    
    // ACCESSORS
    public Menu getViewMenu() {
        return searchViewMenu;
    }
    
    // METHODS
    
    private void buildViewItems() {

        searchViewMenu = new Menu("Toggle search columns");

        numberCheckItem		= new CheckMenuItem("Number");
        titleCheckItem 		= new CheckMenuItem("Title");
        yearCheckItem 		= new CheckMenuItem("Year");
        ratingCheckItem		= new CheckMenuItem("Rating");
        runtimeCheckItem	= new CheckMenuItem("Runtime");
        genresCheckItem		= new CheckMenuItem("Genres");
        directorsCheckItem	= new CheckMenuItem("Directors");
        writersCheckItem	= new CheckMenuItem("Writers");
        actorsCheckItem		= new CheckMenuItem("Actors");
        languagesCheckItem	= new CheckMenuItem("Languages");
        countriesCheckItem	= new CheckMenuItem("Countries");
        metascoreCheckItem	= new CheckMenuItem("Metascore");
        imdbRatingCheckItem	= new CheckMenuItem("IMDB Rating");
        imdbVotesCheckItem	= new CheckMenuItem("IMDB Votes");
        filenameCheckItem	= new CheckMenuItem("Filename");

        searchViewMenu.getItems().addAll(numberCheckItem, titleCheckItem, yearCheckItem,
                ratingCheckItem, runtimeCheckItem, genresCheckItem, directorsCheckItem,
                writersCheckItem, actorsCheckItem, languagesCheckItem, countriesCheckItem,
                metascoreCheckItem, imdbRatingCheckItem, imdbVotesCheckItem, filenameCheckItem);
    }
    
    @SuppressWarnings("unchecked")
    public TableView<Movie> buildMoviesTable(ObservableList<Movie> movies, boolean isGarbage) {
        TableView<Movie> tableView = new TableView<>();

        // BUILD THE TABLECOLUMNS AND SET THEIR PROPERTIES		
        TableColumn<Movie, String>
            numberCol 		= new TableColumn<>("#"),
            titleCol 		= new TableColumn<>("Title"),
            yearCol 		= new TableColumn<>("Year"),
            ratingCol 		= new TableColumn<>("Rating"),
            runtimeCol 		= new TableColumn<>("Runtime"),
            genresCol		= new TableColumn<>("Genres"),
            directorsCol	= new TableColumn<>("Directors"),
            writersCol 		= new TableColumn<>("Writers"),
            actorsCol 		= new TableColumn<>("Actors"),
            languagesCol 	= new TableColumn<>("Languages"),
            countriesCol	= new TableColumn<>("Countries"),
            metascoreCol	= new TableColumn<>("Metascore"),
            imdbRatingCol	= new TableColumn<>("IMDB Rating"),
            imdbVotesCol	= new TableColumn<>("IMDB Votes"),
            filenameCol		= new TableColumn<>("Filename");

        // NUMBERCOL IS AN UNSORTABLE COLUMN OF NUMBERS THAT IS ON THE LEFT SIDE OF THE TABLE
        numberCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Movie, String>, ObservableValue<String>>() {
            @SuppressWarnings("rawtypes")
            @Override public ObservableValue<String> call(TableColumn.CellDataFeatures<Movie, String> p) {
                return new ReadOnlyObjectWrapper((tableView.getItems().indexOf(p.getValue()) + 1) + "");
            }
        });
        numberCol.setSortable(false);

        titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        yearCol.setCellValueFactory(new PropertyValueFactory<>("year"));
        ratingCol.setCellValueFactory(new PropertyValueFactory<>("rating"));
        runtimeCol.setCellValueFactory(new PropertyValueFactory<>("runtime"));
        genresCol.setCellValueFactory(new PropertyValueFactory<>("genres"));
        directorsCol.setCellValueFactory(new PropertyValueFactory<>("directors"));
        writersCol.setCellValueFactory(new PropertyValueFactory<>("writers"));
        actorsCol.setCellValueFactory(new PropertyValueFactory<>("actors"));
        languagesCol.setCellValueFactory(new PropertyValueFactory<>("languages"));
        countriesCol.setCellValueFactory(new PropertyValueFactory<>("countries"));
        metascoreCol.setCellValueFactory(new PropertyValueFactory<>("metascore"));
        imdbRatingCol.setCellValueFactory(new PropertyValueFactory<>("IMDBRating"));
        imdbVotesCol.setCellValueFactory(new PropertyValueFactory<>("IMDBVotes"));
        filenameCol.setCellValueFactory(new PropertyValueFactory<>("filename"));

        // APPLY WIDTH AND CSS FORMATTING TO EACH COLUMN

        numberCol.setPrefWidth(35);
        numberCol.setResizable(false);
        numberCol.setStyle(CSS_ALIGN_RIGHT);
        numberCol.setVisible(numberCheckItem.isSelected());

        titleCol.setMinWidth(200);
        titleCol.setStyle(CSS_ALIGN_LEFT);
        titleCol.setVisible(titleCheckItem.isSelected());
        
        titleCol.setSortType(TableColumn.SortType.ASCENDING);

        yearCol.setPrefWidth(60);
        yearCol.setResizable(false);
        yearCol.setStyle(CSS_ALIGN_CENTER);
        yearCol.setVisible(yearCheckItem.isSelected());

        ratingCol.setPrefWidth(80);
        ratingCol.setResizable(false);
        ratingCol.setStyle(CSS_ALIGN_CENTER);
        ratingCol.setVisible(ratingCheckItem.isSelected());

        runtimeCol.setPrefWidth(80);
        runtimeCol.setResizable(false);
        runtimeCol.setStyle(CSS_ALIGN_CENTER);
        runtimeCol.setVisible(runtimeCheckItem.isSelected());

        genresCol.setMinWidth(200);
        genresCol.setPrefWidth(200);
        genresCol.setStyle(CSS_ALIGN_LEFT);
        genresCol.setVisible(genresCheckItem.isSelected());

        directorsCol.setMinWidth(200);
        directorsCol.setStyle(CSS_ALIGN_LEFT);
        directorsCol.setVisible(directorsCheckItem.isSelected());

        writersCol.setMinWidth(350);
        writersCol.setStyle(CSS_ALIGN_LEFT);
        writersCol.setVisible(writersCheckItem.isSelected());

        actorsCol.setMinWidth(350);
        actorsCol.setStyle(CSS_ALIGN_LEFT);
        actorsCol.setVisible(actorsCheckItem.isSelected());

        languagesCol.setMinWidth(100);
        languagesCol.setStyle(CSS_ALIGN_LEFT);
        languagesCol.setVisible(languagesCheckItem.isSelected());

        countriesCol.setMinWidth(80);
        countriesCol.setStyle(CSS_ALIGN_LEFT);
        countriesCol.setVisible(countriesCheckItem.isSelected());

        metascoreCol.setPrefWidth(70);
        metascoreCol.setResizable(false);
        metascoreCol.setStyle(CSS_ALIGN_CENTER);
        metascoreCol.setVisible(metascoreCheckItem.isSelected());

        imdbRatingCol.setPrefWidth(90);
        imdbRatingCol.setResizable(false);
        imdbRatingCol.setStyle(CSS_ALIGN_CENTER);
        imdbRatingCol.setVisible(imdbRatingCheckItem.isSelected());

        imdbVotesCol.setMinWidth(100);
        imdbVotesCol.setStyle(CSS_ALIGN_LEFT);
        imdbVotesCol.setVisible(imdbVotesCheckItem.isSelected());

        filenameCol.setMinWidth(150);
        filenameCol.setStyle(CSS_ALIGN_LEFT);
        filenameCol.setVisible(filenameCheckItem.isSelected());

        // ATTACH TABLEVIEW HEIGHT TO STAGE HEIGHT
        tableView.prefHeightProperty().bind(app.stage().heightProperty());

        // BUILD THE TABLEVIEW THAT WILL BE RETURNED
        tableView.getColumns().addAll(numberCol, titleCol, yearCol, ratingCol,
                runtimeCol, genresCol, directorsCol, writersCol, actorsCol,
                languagesCol, countriesCol, metascoreCol, imdbRatingCol,
                imdbVotesCol, filenameCol);

        tableView.setItems(movies);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tableView.getSortOrder().add(titleCol);

        // SET LISTENERS FOR VISIBILITY OF COLUMNS
        numberCheckItem.selectedProperty().addListener(e -> {
            numberCol.setVisible(numberCheckItem.isSelected());
        });

        titleCheckItem.selectedProperty().addListener(e -> {
            titleCol.setVisible(titleCheckItem.isSelected());
        });

        yearCheckItem.selectedProperty().addListener(e -> {
            yearCol.setVisible(yearCheckItem.isSelected());
        });

        ratingCheckItem.selectedProperty().addListener(e -> {
            ratingCol.setVisible(ratingCheckItem.isSelected());
        });

        runtimeCheckItem.selectedProperty().addListener(e -> {
            runtimeCol.setVisible(runtimeCheckItem.isSelected());
        });

        genresCheckItem.selectedProperty().addListener(e -> {
            genresCol.setVisible(genresCheckItem.isSelected());
        });

        directorsCheckItem.selectedProperty().addListener(e -> {
            directorsCol.setVisible(directorsCheckItem.isSelected());
        });

        writersCheckItem.selectedProperty().addListener(e -> {
            writersCol.setVisible(writersCheckItem.isSelected());
        });

        actorsCheckItem.selectedProperty().addListener(e -> {
            actorsCol.setVisible(actorsCheckItem.isSelected());
        });

        languagesCheckItem.selectedProperty().addListener(e -> {
            languagesCol.setVisible(languagesCheckItem.isSelected());
        });

        countriesCheckItem.selectedProperty().addListener(e -> {
            countriesCol.setVisible(countriesCheckItem.isSelected());
        });

        metascoreCheckItem.selectedProperty().addListener(e -> {
            metascoreCol.setVisible(metascoreCheckItem.isSelected());
        });

        imdbRatingCheckItem.selectedProperty().addListener(e -> {
            imdbRatingCol.setVisible(imdbRatingCheckItem.isSelected());
        });

        imdbVotesCheckItem.selectedProperty().addListener(e -> {
            imdbVotesCol.setVisible(imdbVotesCheckItem.isSelected());
        });

        filenameCheckItem.selectedProperty().addListener(e -> {
            filenameCol.setVisible(filenameCheckItem.isSelected());
        });
        
        setViewSettings(app.getSettingsComponent().getViewSettings());
        
        
        
        // ADD LISTENER FOR SELECTION CHANGE
        tableView.getSelectionModel().selectedItemProperty().addListener(e -> {
            Movie clickedMovie = tableView.getSelectionModel().getSelectedItem();
                
            if(clickedMovie != null) {
                if(isGarbage) {
                    tableView.setContextMenu(buildGarbageContextMenu(clickedMovie));
                } else {
                    tableView.setContextMenu(buildMoviesContextMenu(clickedMovie));
                }
            }
        });
        
        return tableView;
    }
    
    public TableView<Movie> buildErrorsTable(ObservableList<Movie> errors) {
        
        TableView<Movie> tableView = new TableView<>();
        
        TableColumn<Movie, String>
                numberCol = new TableColumn<>("#"),
                filenameCol = new TableColumn<>("Filename"),
                filepathCol = new TableColumn<>("Filepath");
        
        // NUMBERCOL IS AN UNSORTABLE COLUMN OF NUMBERS THAT IS ON THE LEFT SIDE OF THE TABLE
        numberCol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Movie, String>, ObservableValue<String>>() {
            @SuppressWarnings("rawtypes")
            @Override public ObservableValue<String> call(TableColumn.CellDataFeatures<Movie, String> p) {
                return new ReadOnlyObjectWrapper((tableView.getItems().indexOf(p.getValue()) + 1) + "");
            }
        });
        numberCol.setSortable(false);
        
        filenameCol.setCellValueFactory(new PropertyValueFactory<>("filename"));
        filepathCol.setCellValueFactory(new PropertyValueFactory<>("filepath"));
        
        // SET FORMATTING AND STYLE
        numberCol.setPrefWidth(35);
        numberCol.setResizable(false);
        numberCol.setStyle(CSS_ALIGN_RIGHT);
        
        filenameCol.setMinWidth(300);
        filenameCol.setSortType(TableColumn.SortType.ASCENDING);
        
        filepathCol.setMinWidth(350);
        
        // ATTACH TABLEVIEW HEIGHT TO STAGE HEIGHT
        tableView.prefHeightProperty().bind(app.stage().heightProperty());

        // BUILD THE TABLEVIEW THAT WILL BE RETURNED
        tableView.getColumns().addAll(numberCol, filenameCol, filepathCol);

        tableView.setItems(errors);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        tableView.getSortOrder().add(filenameCol);
        
        return tableView;
    }
    
    public ContextMenu buildMoviesContextMenu(Movie clickedMovie) {
        ContextMenu context = new ContextMenu();
        
        MenuItem openItem = new MenuItem("Open");
        MenuItem openIMDBItem = new MenuItem("Open in browser");
        MenuItem playItem = new MenuItem("Play movie");
        MenuItem correctDataItem = new MenuItem("Correct data");
        MenuItem removeItem = new MenuItem("Remove");
        
        // BUILD COMPONENT HANDLERS
        WorkspaceController workCont = new WorkspaceController(app);
        TabPaneController tabPaneCont = new TabPaneController(app);
        
        // SET HANDLERS
        openItem.setOnAction(e -> {
            workCont.processOpenMovieTab(clickedMovie);
            context.hide();
        });
        
        openIMDBItem.setOnAction(e -> {
            MainApplication.processOpenLink("http://www.imdb.com/title/" + clickedMovie.getIMDBid());
            context.hide();
        });
        
        playItem.setOnAction(e -> {
            tabPaneCont.processPlayMovie(clickedMovie);
            context.hide();
        });
        
        correctDataItem.setOnAction(e -> {
            tabPaneCont.processCorrectData(clickedMovie, false);
            context.hide();
        });
        
        removeItem.setOnAction(e -> {
            app.trace("Removing movie '" + clickedMovie.getTitle() + "'");
            app.getDataComponent().addGarbage(clickedMovie);
            app.getDataComponent().removeMovie(clickedMovie);
            
            app.getWorkspaceComponent().refreshAndSaveMovies();
            
            context.hide();
        });
        
        context.getItems().addAll(openItem, openIMDBItem, playItem,
                new SeparatorMenuItem(), correctDataItem, new SeparatorMenuItem(), removeItem);
        
        return context;
    }
    
    public ContextMenu buildErrorsContextMenu(Movie clickedError) {
        ContextMenu context = new ContextMenu();
        
        MenuItem correctDataItem = new MenuItem("Correct data");
        
        // BUILD CONTROLLER
        TabPaneController tabPaneCont = new TabPaneController(app);
        
        correctDataItem.setOnAction(e -> {
            tabPaneCont.processCorrectData(clickedError, true);
            context.hide();
        });
        
        context.getItems().add(correctDataItem);
        
        return context;
    }
    
    public ContextMenu buildGarbageContextMenu(Movie clickedMovie) {
        ContextMenu context = new ContextMenu();
        
        MenuItem restoreItem = new MenuItem("Restore");
        
        restoreItem.setOnAction(e -> {
            app.trace("Restoring movie '" + clickedMovie.getTitle() + "'");
            app.getDataComponent().addMovie(clickedMovie);
            app.getDataComponent().removeGarbage(clickedMovie);
            
            app.getWorkspaceComponent().refreshAndSaveMovies();
            
            context.hide();
        });
        
        context.getItems().add(restoreItem);
        
        return context;
    }
    
    public void setViewSettings(String settings) {
        int index = 0;
        
        numberCheckItem.setSelected(isOne(settings, index++));
        titleCheckItem.setSelected(isOne(settings, index++));
        yearCheckItem.setSelected(isOne(settings, index++));
        ratingCheckItem.setSelected(isOne(settings, index++));
        runtimeCheckItem.setSelected(isOne(settings, index++));
        genresCheckItem.setSelected(isOne(settings, index++));
        directorsCheckItem.setSelected(isOne(settings, index++));
        writersCheckItem.setSelected(isOne(settings, index++));
        actorsCheckItem.setSelected(isOne(settings, index++));
        languagesCheckItem.setSelected(isOne(settings, index++));
        countriesCheckItem.setSelected(isOne(settings, index++));
        metascoreCheckItem.setSelected(isOne(settings, index++));
        imdbRatingCheckItem.setSelected(isOne(settings, index++));
        imdbVotesCheckItem.setSelected(isOne(settings, index++));
        filenameCheckItem.setSelected(isOne(settings, index++));
    }
    
    public String getViewSettings() {
        String output = "";
        
        output += boolToBin(numberCheckItem.isSelected());
        output += boolToBin(titleCheckItem.isSelected());
        output += boolToBin(yearCheckItem.isSelected());
        output += boolToBin(ratingCheckItem.isSelected());
        output += boolToBin(runtimeCheckItem.isSelected());
        output += boolToBin(genresCheckItem.isSelected());
        output += boolToBin(directorsCheckItem.isSelected());
        output += boolToBin(writersCheckItem.isSelected());
        output += boolToBin(actorsCheckItem.isSelected());
        output += boolToBin(languagesCheckItem.isSelected());
        output += boolToBin(countriesCheckItem.isSelected());
        output += boolToBin(metascoreCheckItem.isSelected());
        output += boolToBin(imdbRatingCheckItem.isSelected());
        output += boolToBin(imdbVotesCheckItem.isSelected());
        output += boolToBin(filenameCheckItem.isSelected());
        
        return output;
    }
    
    // HELPER METHODS
    private boolean isOne(String string, int index) {
        return string.charAt(index) == '1';
    }
    
    private String boolToBin(boolean bool) {
        if(bool) { return "1"; }
        return "0";
    }
}
