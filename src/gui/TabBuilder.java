package gui;

import application.MainApplication;
import controllers.TabPaneController;
import static css.StyleConstants.*;
import data.Movie;
import java.io.File;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import strings.StringParser;

/**
 *
 * @author Nathan
 */
public class TabBuilder {
    // ATTRIBUTES
    MainApplication app;
    
    // CONSTRUCTORS
    public TabBuilder(MainApplication app) {
        this.app = app;
    }
    
    // METHODS
    
    public Tab buildSearchTab(String searchQuery) {

        // BUILD TABLE FROM SEARCH RESULTS
        TableView<Movie> searchTable = app.getWorkspaceComponent().addMoviesTable(app.getDataComponent().getSearchResults(), false);

        // BUILD SEARCH TAB AND ADD CONTENT
        Tab newSearchTab = new Tab(searchQuery);
        newSearchTab.setContent(searchTable);
        newSearchTab.setClosable(true);
        
        newSearchTab.setContextMenu(tabContextMenu(newSearchTab));

        // ADD NEW SEARCH TAB TO TABPANE
        return newSearchTab;
    }
    
    public Tab buildGarbageTab() {
        // BUILD TABLE FROM SEARCH RESULTS
        TableView<Movie> garbageTable = app.getWorkspaceComponent().addMoviesTable(app.getDataComponent().getGarbage(), true);

        // BUILD SEARCH TAB AND ADD CONTENT
        Tab garbageTab = new Tab("Garbage");
        garbageTab.setContent(garbageTable);
        garbageTab.setClosable(true);
        
        garbageTab.setContextMenu(tabContextMenu(garbageTab));

        // ADD NEW SEARCH TAB TO TABPANE
        return garbageTab;
    }

    public Tab buildMovieTab(Movie movie) {
        
        // BUILD CONTROLLER
        TabPaneController tabPaneCont = new TabPaneController(app);

        // GET POSTER IMAGE
        ImageView posterImageView = new ImageView(getPoster(movie));

        posterImageView.setOpacity(0.9);
        posterImageView.setCursor(Cursor.HAND);

        // ADD MOUSE OVER EFFECTS
        posterImageView.setOnMouseEntered(e -> {
            posterImageView.setOpacity(1);
        });

        posterImageView.setOnMouseExited(e -> {
            posterImageView.setOpacity(0.9);
        });

        // ADD EVENT HANDLER TO OPEN MOVIE FILE
        posterImageView.setOnMouseClicked(e -> {
            tabPaneCont.processPlayMovie(movie);
        });

        // BUILD LABELS FROM MOVIE INFO
        Label
            titleLabel 		= new Label(movie.getTitle()),
            yearLabel		= new Label(movie.getYear()),
            ratingLabel 	= new Label(movie.getRating()),
            runtimeLabel 	= new Label(movie.getRuntime()),
            genresLabel		= new Label("Genres: "),
            genresTextLabel	= new Label(movie.getGenres()),
            directorsLabel	= new Label("Directors: "),
            directorsTextLabel  = new Label(movie.getDirectors()),
            writersLabel	= new Label("Writers: "),
            writersTextLabel    = new Label(movie.getWriters()),
            actorsLabel		= new Label("Actors: "),
            actorsTextLabel     = new Label(movie.getActors()),
            plotLabel		= new Label("Plot: "),
            plotTextLabel       = new Label(movie.getPlot()),
            languageLabel	= new Label("Languages: " + movie.getLanguages()),
            countryLabel	= new Label("Countries: " + movie.getCountries()),
            metascoreLabel	= new Label("Metascore: " + movie.getMetascore()),
            imdbRatingLabel	= new Label("IMDB Rating: " + movie.getIMDBRating()),
            imdbVotesLabel	= new Label("IMDB Votes: " + movie.getIMDBVotes()),
            filenameLabel	= new Label("\"" + movie.getFilename() + "\"");

        // BUILD LARGER TOP LABELS
        titleLabel.setStyle(CSS_BOLD + CSS_FONT_SIZE + "40;");
        titleLabel.setWrapText(true);

        yearLabel.setStyle(CSS_FONT_SIZE + "22;");
        ratingLabel.setStyle(CSS_FONT_SIZE + "22;");
        runtimeLabel.setStyle(CSS_FONT_SIZE + "22;");

        HBox topInfoHBox = new HBox(yearLabel, ratingLabel, runtimeLabel);
        topInfoHBox.setSpacing(20);
        topInfoHBox.setPadding(new Insets(LABEL_PADDING));
        
        // BUILD GENRES LABEL SECTION
        genresLabel.setStyle(CSS_FONT_SIZE + "14;");
        genresLabel.setMinWidth(65);
        genresTextLabel.setStyle(CSS_FONT_SIZE + "14;");
        genresTextLabel.setWrapText(true);
        
        HBox genresHBox = new HBox(genresLabel, genresTextLabel);
        
        // BUILD DIRECTORS LABEL SECTION
        directorsLabel.setStyle(CSS_FONT_SIZE + "14;");
        directorsLabel.setMinWidth(65);
        directorsTextLabel.setStyle(CSS_FONT_SIZE + "14;");
        directorsTextLabel.setWrapText(true);
        
        HBox directorsHBox = new HBox(directorsLabel, directorsTextLabel);
        
        // BUILD WRITERS LABEL SECTION
        writersLabel.setStyle(CSS_FONT_SIZE + "14;");
        writersLabel.setMinWidth(65);
        writersTextLabel.setStyle(CSS_FONT_SIZE + "14;");
        writersTextLabel.setWrapText(true);
        
        HBox writersHBox = new HBox(writersLabel, writersTextLabel);
        
        // BUILD ACTORS LABEL SECTION
        actorsLabel.setStyle(CSS_FONT_SIZE + "14;");
        actorsLabel.setMinWidth(65);
        actorsTextLabel.setStyle(CSS_FONT_SIZE + "14;");
        actorsTextLabel.setWrapText(true);
        
        HBox actorsHBox = new HBox(actorsLabel, actorsTextLabel);

        // BUILD PLOT LABELS SECTION
        plotLabel.setStyle(CSS_FONT_SIZE + "14;");
        plotLabel.setMinWidth(40);
        plotTextLabel.setStyle(CSS_FONT_SIZE + "14;");
        plotTextLabel.setWrapText(true);
        
        HBox plotHBox = new HBox(plotLabel, plotTextLabel);
        plotHBox.setAlignment(Pos.TOP_LEFT);
        
        // ADD ALL THE TOP ELEMENTS
        VBox topVBox = new VBox(titleLabel, topInfoHBox, genresHBox, spacer(),
                        directorsHBox, writersHBox, actorsHBox, spacer(), plotHBox);
        topVBox.setAlignment(Pos.TOP_LEFT);
        VBox.setVgrow(topVBox, Priority.ALWAYS);

        // BUILD BOTTOM  RIGHT SECTION
        languageLabel.setStyle(CSS_FONT_SIZE + "14;");
        countryLabel.setStyle(CSS_FONT_SIZE + "14;");

        VBox areaVBox = new VBox(spacer(), languageLabel, countryLabel);
        areaVBox.setAlignment(Pos.CENTER_RIGHT);

        HBox bottomRightHBox = new HBox(areaVBox);
        bottomRightHBox.setAlignment(Pos.CENTER_RIGHT);
        HBox.setHgrow(bottomRightHBox, Priority.ALWAYS);

        // BUILD THE BOTTOM LEFT SECTION
        metascoreLabel.setStyle(CSS_FONT_SIZE + "14;");
        imdbRatingLabel.setStyle(CSS_FONT_SIZE + "14;");
        imdbVotesLabel.setStyle(CSS_FONT_SIZE + "14;");

        VBox scoresVBox = new VBox(metascoreLabel, imdbRatingLabel, imdbVotesLabel);

        HBox bottomLeftHBox = new HBox(scoresVBox);
        bottomLeftHBox.setAlignment(Pos.CENTER_LEFT);
        HBox.setHgrow(bottomLeftHBox, Priority.ALWAYS);

        HBox bottomHBox = new HBox(bottomLeftHBox, bottomRightHBox);

        // BUILD CORRECTDATA BUTTON AND FILENAME LABEL
        Button correctDataButton = new Button("Correct Data");
        
        filenameLabel.setStyle(CSS_FONT_SIZE + "14;");
        
        // BUILD CONTAINER FOR BUTTON AND FILENAME LABEL
        HBox buttonHBox = new HBox(15);
        buttonHBox.getChildren().addAll(correctDataButton, filenameLabel);
        buttonHBox.setAlignment(Pos.CENTER_LEFT);

        // ADD THE ELEMENTS TO THE BOTTOM VBOX
        VBox bottomVBox = new VBox(bottomHBox, spacer(), buttonHBox);
        bottomVBox.setAlignment(Pos.BOTTOM_CENTER);
        VBox.setVgrow(bottomVBox, Priority.ALWAYS);

        // BUILD INFO VBOX AND ADD LABELS
        VBox infoVBox = new VBox(topVBox, bottomVBox);

        infoVBox.setPadding(new Insets(20));
        VBox.setVgrow(infoVBox, Priority.ALWAYS);

        // BUILD CONTAINING MOVIE HBOX WITH COMPONENTS AND PLACE INSIDE TAB
        HBox movieHBox = new HBox(posterImageView, infoVBox);
        movieHBox.setPadding(new Insets(LABEL_PADDING));

        // BUILD MOVIE TAB AND ADD CONTENT
        Tab newMovieTab = new Tab(movie.getTitle());
        newMovieTab.setContent(movieHBox);
        newMovieTab.setClosable(true);
        
        newMovieTab.setContextMenu(tabContextMenu(newMovieTab));
        
        // SET HANDLER FOR CORRECTDATA BUTTON
        correctDataButton.setOnAction(e -> {
            tabPaneCont.processCorrectData(movie, false);
        });

        // RETURN NEW TAB
        return newMovieTab;
    }
    
    public Tab buildErrorsTab() {
        
        TableView<Movie> errorsTable = app.getWorkspaceComponent().addErrorsTable(app.getDataComponent().getErrors());
        
        // BUILD MOVIE TAB AND ADD CONTENT
        Tab newErrorsTab = new Tab("Errors");
        newErrorsTab.setContent(errorsTable);
        newErrorsTab.setClosable(true);
        
        newErrorsTab.setContextMenu(tabContextMenu(newErrorsTab));
        
        // RETURN NEW TAB
        return newErrorsTab;
    }
    
    public ContextMenu tabContextMenu(Tab clickedTab) {
        
        // BUILD CONTROLLER
        TabPaneController tabPaneCont = new TabPaneController(app);
        
        ContextMenu context = new ContextMenu();
        
        MenuItem closeAllItem, closeOtherItem;
        
        if(clickedTab.isClosable()) {
            MenuItem closeItem = new MenuItem("Close");

            closeItem.setOnAction(e -> {
                tabPaneCont.processCloseTab(clickedTab);
                context.hide();
            });
            
            context.getItems().add(closeItem);
        }
        
        closeAllItem = new MenuItem("Close All");
        
        closeAllItem.setOnAction(e -> {
            tabPaneCont.processCloseAllTabs();
            context.hide();
        });
        
        closeOtherItem = new MenuItem("Close Other");
        
        closeOtherItem.setOnAction(e -> {
            tabPaneCont.processCloseOtherTabs(clickedTab);
            context.hide();
        });
        
        context.getItems().addAll(closeAllItem, closeOtherItem);
        
        return context;
    }
    
    public Image getPoster(Movie movie) {
        String posterImageFilepath = app.getWorkspaceComponent().getPostersPath() + "\\"
                + StringParser.removeExtension(movie.getFilename())
                + "." + StringParser.getExtension(movie.getPoster());

        if(new File(posterImageFilepath).exists()) {
            return new Image("file:" + posterImageFilepath);
        } else {
            return new Image("file:./images/image_not_found.png");
        }
    }
    
    // PRIVATE HELPERS
    
    private Label spacer() {
        return new Label("");
    }
}
