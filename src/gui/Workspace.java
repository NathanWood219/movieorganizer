package gui;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

import application.MainApplication;
import controllers.WorkspaceController;
import data.Movie;
import data.MovieFilenameComparator;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import static css.StyleConstants.*;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import org.controlsfx.control.textfield.TextFields;
import updater.UpdaterProperties;

/**
 * The <code>Workspace</code> class constructs the GUI, applies CSS, and attaches handlers to controls.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 1, 2016
 *
 */
public class Workspace {
    // CONSTANTS
    private final String
            UPDATER_LINK    = "http://moviecaboodle.com/update",
            UPDATER_TARGET  = "./Caboodle.jar";

    // ATTRIBUTES
    MainApplication app;
    
    TableComponent tableComponent;
    TabBuilder tabBuilder;

    final String SAVE_DIR_PATH, SAVE_DATA, SETTINGS_DATA, POSTERS_DIR_PATH;

    VBox mainVBox;

    MenuBar menuBar;
    Menu fileMenu, viewMenu, helpMenu;

    MenuItem scanItem, openErrorsItem, openGarbageItem, exitItem;                                       // FILE MENU ITEMS

    Menu searchViewMenu;                                                                                // SEARCH COLUMNS VIEW MENU (SUB-MENU)
    MenuItem searchHelpItem, bugReportItem, updatesItem, aboutItem;					// HELP MENU ITEMS

    TextField searchField;
    Button searchButton;
    ProgressBar progressBar;
    Label progressLabel;
    Button donateButton;

    TabPane tabPane;
    Tab moviesTab;

    TableView<Movie> moviesTableView;

    // CONSTRUCTORS
    public Workspace(MainApplication app) {
        this.app = app;
        this.mainVBox = (VBox)app.scene().getRoot();
        
        SAVE_DIR_PATH   = "./data";
        SAVE_DATA       = "moviesData.json";
        SETTINGS_DATA   = "settings.json";
        POSTERS_DIR_PATH= "./Posters";
        
        // NOW THAT THE TABPANE HAS BEEN CONSTRUCTED, WE CAN INITIALIZE THE TAB RELATED CLASSES
        tableComponent = new TableComponent(app);
        tabBuilder = new TabBuilder(app);
        
        mainVBox.prefHeightProperty().bind(app.stage().heightProperty());

        // LOAD SETTINGS, FIRST MAKING SURE THAT THE DIRECTORIES EXIST
        boolean foundSettings = loadSettings();
        loadData();

        // CONSTRUCT GUI, SET EVENT HANDLERS/LISTENERS, AND ADD CSS STYLE
        buildGUI();
        setHandlers();
        initStyle();
        
        // NOW LOAD IN VIEW SETTINGS AFTER EVERYTHING HAS BEEN BUILT
        loadViewSettings(app.getSettingsComponent().getViewSettings());
        
        // SAVE UPDATER PROPERTIES
        app.trace("Saving updater properties...");
        // SAVE UPDATER PROPERTIES
        UpdaterProperties props = new UpdaterProperties();

        props.setLink(UPDATER_LINK);
        props.setTarget(UPDATER_TARGET);

        try {
            props.saveProperties();
        } catch(IOException e) {
            app.trace("Failed to save the updater properties!");
            app.traceEx(e);
        }
        
        // CHECK FOR UPDATES
        WorkspaceController workCont = new WorkspaceController(app);
        workCont.processUpdate(UPDATER_LINK, false);
        
        // CHECK IF SCAN NEEDS TO BE DONE
        if(!foundSettings) {
            WindowBuilder.buildScanPromptWindow(app);
        }

    }

    // ACCESSORS
    public final String getSavePath() {
        return SAVE_DIR_PATH + "/" + SAVE_DATA;
    }
    
    public final String getSettingsPath() {
        return SAVE_DIR_PATH + "/" + SETTINGS_DATA;
    }

    public final String getPostersPath() {
        return POSTERS_DIR_PATH;
    }
    
    public ProgressBar getProgressBar() {
        return progressBar;
    }
    
    public Label getProgressLabel() {
        return progressLabel;
    }
    
    public TabPane getTabPane() {
        return tabPane;
    }
    
    public Tab getMoviesTab() {
        return moviesTab;
    }

    // METHODS

    public final void buildGUI() {

        // CREATE AND BUILD FILE MENU		
        scanItem = new MenuItem("Scan movies");
        openErrorsItem = new MenuItem("Open errors list");
        openGarbageItem = new MenuItem("Open garbage list");
        exitItem = new MenuItem("Exit");

        fileMenu = new Menu("File");
        fileMenu.getItems().addAll(scanItem, separatorItem(), openErrorsItem, 
                openGarbageItem, separatorItem(), exitItem);

        // CREATE AND BUILD VIEW MENU
        viewMenu = new Menu("View");
        viewMenu.getItems().add(tableComponent.getViewMenu());

        // CREATE AND BUILD HELP MENU
        searchHelpItem = new MenuItem("Search help");
        bugReportItem = new MenuItem("Report a bug");
        updatesItem = new MenuItem("Check for updates");
        aboutItem = new MenuItem("About");

        helpMenu = new Menu("Help");
        helpMenu.getItems().addAll(searchHelpItem, bugReportItem, 
                updatesItem, separatorItem(), aboutItem);


        // ADD MENUS TO MENUBAR
        menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu, viewMenu, helpMenu);

        // ADD MENUBAR TO MAIN VBOX
        mainVBox.getChildren().add(menuBar);

        // ADD PROGRESS BAR AND SEARCH FIELD STUFF
        searchField = TextFields.createClearableTextField();
        searchField.setMinWidth(200);
        searchButton = new Button("Search");

        progressBar = new ProgressBar(0F);
        progressBar.setMinWidth(300);
        progressBar.setVisible(false);
        
        // CREATE PROGRESS LABEL THAT STORES STATUS OF SCAN
        progressLabel = new Label();
        progressLabel.setVisible(false);
        
        StackPane progressStackPane = new StackPane(progressBar, progressLabel);

        donateButton = new Button("Donate");

        // CONTAIN THEM WITHIN THE SAME ROW
        HBox leftHBox = new HBox(searchField, searchButton);
        leftHBox.setSpacing(4);
        leftHBox.setPadding(new Insets(2));
        
        HBox rightHBox = new HBox(donateButton);

        HBox.setHgrow(leftHBox, Priority.ALWAYS);
        HBox.setHgrow(rightHBox, Priority.ALWAYS);

        leftHBox.setAlignment(Pos.CENTER_LEFT);
        rightHBox.setAlignment(Pos.CENTER_RIGHT);

        HBox topHBox = new HBox(leftHBox, progressStackPane, rightHBox);

        topHBox.setPadding(new Insets(CONTROL_PADDING));

        topHBox.setAlignment(Pos.CENTER);

        // ADD TOPHBOX TO MAIN VBOX
        mainVBox.getChildren().add(topHBox);

        // BUILD TABPANE AND ADD DEFAULT UNCLOSABLE MOVIES TAB
        moviesTab = new Tab("Movies");
        moviesTab.setClosable(false);
        moviesTab.setContextMenu(tabBuilder.tabContextMenu(moviesTab));

        // CONSTRUCT MOVIES TABLE AND PLACE INSIDE SEARCH TAB
        moviesTableView = addMoviesTable(app.getDataComponent().getMovies(), false);

        moviesTab.setContent(moviesTableView);

        // INITIALIZE THE TAB PANE HERE IN ORDER TO CREATE THE RELATED CLASSES BELOW
        tabPane = new TabPane();
        tabPane.getTabs().add(moviesTab);

        // BIND TABPANE HEIGHT TO WINDOW HEIGHT PLUS APPLY FORMATTING
        tabPane.prefHeightProperty().bind(app.stage().heightProperty());
        tabPane.setPadding(new Insets(CONTROL_PADDING));

        // ADD TABPANE TO THE MAIN VBOX
        mainVBox.getChildren().add(tabPane);
    }
    
    // PRIVATE HELPER METHOD FOR MENUS
    private SeparatorMenuItem separatorItem() {
        return new SeparatorMenuItem();
    }

    public final void setHandlers() {

        WorkspaceController workCont = new WorkspaceController(app);
                
        // SET LISTENER FOR WHEN EXITING
        app.stage().onCloseRequestProperty().addListener(e -> { workCont.processExit(); });

        // SET EVENT HANDLERS FOR FILE MENU ITEMS
        scanItem.setOnAction(e -> { WindowBuilder.buildScanPromptWindow(app); });
        openErrorsItem.setOnAction(e -> { workCont.processOpenErrorsList(); });
        openGarbageItem.setOnAction(e -> { workCont.processOpenGarbageList(); });
        exitItem.setOnAction(e -> { workCont.processExit(); });

        // SET EVENT HANDLERS FOR HELP MENU ITEMS
        searchHelpItem.setOnAction(e -> { WindowBuilder.buildHelpWindow(app); });
        bugReportItem.setOnAction(e -> { WindowBuilder.buildBugReportWindow(app); });
        updatesItem.setOnAction(e -> { workCont.processUpdate(UPDATER_LINK, true); });
        aboutItem.setOnAction(e -> { WindowBuilder.buildAboutWindow(app); });


        // SET EVENT HANDLERS AND LISTENERS FOR TOPHBOX
        searchField.setOnKeyPressed(e -> {
            if(e.getCode() == KeyCode.ENTER && !searchField.getText().isEmpty()) {
                workCont.processSearch(searchField.getText());
            }
        });

        searchButton.setOnAction(e -> {
            if(!searchField.getText().isEmpty()) {
                workCont.processSearch(searchField.getText());
            }
        });

        donateButton.setOnAction(e -> { MainApplication.processOpenLink("http://paypal.me/nightsprol"); });
        
    }

    public final void initStyle() {
        app.scene().getStylesheets().add("css/style.css");
    }
    
    public TableView<Movie> addMoviesTable(ObservableList<Movie> movies, boolean isGarbage) {
        
        // LET THE BUILDER CONSTRUCT THE TABLEVIEW
        TableView<Movie> tableView = tableComponent.buildMoviesTable(movies, isGarbage);
        
        // HANDLING DOUBLE CLICK ON MOVIE TO OPEN NEW TAB
        tableView.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Movie movie = tableView.getSelectionModel().getSelectedItem();
                
                if(movie != null) {
                    WorkspaceController workCont = new WorkspaceController(app);
                    workCont.processOpenMovieTab(movie);
                }
            }
        });
        
        return tableView;
    }
    
    public TableView<Movie> addErrorsTable(ObservableList<Movie> errors) {
        
        // LET THE BUILDER CONSTRUCT THE TABLEVIEW
        TableView<Movie> tableView = tableComponent.buildErrorsTable(errors);
        
        // HANDLING DOUBLE CLICK ON ITEM TO CORRECT DATA
        tableView.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Movie error = tableView.getSelectionModel().getSelectedItem();

                // VERIFY THAT A STRING WAS CLICKED ON, IF SO, OPEN CORRECT DATA WINDOW
                if(error != null) {
                    try {
                        WindowBuilder.buildCorrectMovieWindow(app, error, true);
                    } catch(Exception ex) {
                        app.traceEx(ex);
                    }
                }
            }
        });
        
        return tableView;
    }
    
    
    
    // ALL BECAUSE CARRYING THE TABPANE ISN'T WORKING
    public ObservableList<Tab> getTabs() {
        return tabPane.getTabs();
    }
    
    public void addTab(Tab tab) {
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
    }
    
    public void addTabFromMovie(Movie movie) {
        addTab(tabBuilder.buildMovieTab(movie));
    }
    
    public void removeTab(Tab tab) {
        tabPane.getTabs().remove(tab);
    }
    
    public void removeTabFromMovie(Movie movie) {
        for(Tab tab : tabPane.getTabs()) {
            if(tab.getText().equals(movie.getTitle())) {
                removeTab(tab);
                break;
            }
        }
    }
    
    public void selectTab(Tab tab) {
        tabPane.getSelectionModel().select(tab);
    }
    // DONE WITH THE TABPANE CRAP
    
    
    
    public void refreshAndSaveMovies() {
        app.trace("\nRefreshing movies list...");
        moviesTableView.refresh();
        
        app.trace("Sorting movies list by title...");
        moviesTableView.sort();
        
        try {
            app.trace("Saving movies data...");
            app.getFileComponent().saveData(app.getDataComponent(), getSavePath());
            app.trace("Data was successfully saved.\n");
        } catch(Exception e) {
            app.trace("Failed to save movies data.");
            app.traceEx(e);
        }
    }
    
    // LOAD THE COLUMN VIEW SETTINGS FROM A BINARY STRING
    public void loadViewSettings(String viewSettings) {
        tableComponent.setViewSettings(viewSettings);
    }
    
    public String getViewSettings() {
        return tableComponent.getViewSettings();
    }

    public void scanStarted() {

        // DISABLE OTHER SCAN-RELATED BUTTONS
        scanItem.setDisable(true);

        // SORT BY FILENAME
        Collections.sort(app.getDataComponent().getMovies(), new MovieFilenameComparator());
    }

    public void scanFinished() {

        // ENABLE OTHER SCAN-RELATED BUTTONS
        scanItem.setDisable(false);

        // REFRESH, SORT, AND SAVE
        refreshAndSaveMovies();
    }

    public void searchStarted() {

        // DISABLE OTHER SEARCH-RELATED BUTTONS
        searchField.setDisable(true);
        searchButton.setDisable(true);
    }

    public void searchFinished() {
        
        // ENABLE OTHER SEARCH-RELATED BUTTONS
        searchField.setDisable(false);
        searchButton.setDisable(false);
    }
    
    // LOAD SETTINGS (NORMALLY ON STARTUP)
    public boolean loadSettings() {
        
        // MAKE SURE DIRECTORY EXISTS
        File dataDir = new File(SAVE_DIR_PATH);
        
        if(!dataDir.exists()) {
            dataDir.mkdir();
        }
        
        // LOAD SETTINGS DATA
        try {
            app.trace("\nLoading settings data...");
            app.getFileComponent().loadSettings(app.getSettingsComponent(), getSettingsPath());
            app.trace("Settings successfully loaded.\n");
            return true;
        } catch(IOException e) {
            app.trace("No settings data found.\n");
        } catch(Exception e) {
            app.trace("Exception occurred, possibly trying to load new settings data.");
            app.traceEx(e);
            WindowBuilder.buildBugWindow(app, "An error occurred while trying to load the settings.");
        }
        
        return false;
    }
    
    public boolean loadData() {
        
        // MAKE SURE DIRECTORIES EXIST
        File postersDir = new File(POSTERS_DIR_PATH);
        File dataDir = new File(SAVE_DIR_PATH);
        
        if(!postersDir.exists()) {
            postersDir.mkdir();
        }
        
        if(!dataDir.exists()) {
            dataDir.mkdir();
        }
        
        // LOAD SAVE DATA
        try {
            app.trace("\nLoading save data...");
            app.getFileComponent().loadData(app.getDataComponent(), getSavePath());
            app.trace("Save data successfully loaded.\n");
            return true;
        } catch(IOException e) {
            app.trace("No save data found.\n");
        } catch(Exception e) {
            app.trace("Exception occurred, possibly trying to load new save data.");
            app.traceEx(e);
            WindowBuilder.buildBugWindow(app, "An error occurred while trying to load the movies data.");
        }
        
        return false;
    }
}
