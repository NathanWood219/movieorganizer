package settings;

import application.MainApplication;

/**
 * The <code>SettingsManager</code> class handles all the settings storage and access for the MovieOrganizer program.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 10, 2016
 * 
 */
public class SettingsManager {
    // ATTRIBUTES
    MainApplication app;
    
    String moviesDirPath;
    String viewColumns;
    
    boolean scanNested;
    
    // CONSTRUCTORS
    public SettingsManager(MainApplication app) {
        this.app = app;
        
        // SET INITIAL VALUES
        moviesDirPath   = "C:\\";
        viewColumns     = "111111111000000";
        scanNested      = false;
    }
    
    // ACCESSORS
    public String getMoviesPath() {
        return moviesDirPath;
    }
    
    public String getViewSettings() {
        return viewColumns;
    }
    
    public boolean getScanNested() {
        return scanNested;
    }
    
    public double getWindowX() {
        return app.stage().getX();
    }
    
    public double getWindowY() {
        return app.stage().getY();
    }
    
    public double getWindowWidth() {
        return app.stage().getWidth();
    }
    
    public double getWindowHeight() {
        return app.stage().getHeight();
    }
    
    public boolean getWindowMaximized() {
        return app.stage().isMaximized();
    }
    
    // MUTATORS
    public void setMoviesPath(String moviesPath) {
        moviesDirPath = moviesPath;
    }
    
    public void setViewSettings(String viewSettings) {
        viewColumns = viewSettings;
    }
    
    public void setScanNested(boolean scanNested) {
        this.scanNested = scanNested;
    }
    
    public void setWindowX(double x) {
        app.stage().setX(x);
    }
    
    public void setWindowY(double y) {
        app.stage().setY(y);
    }
    
    public void setWindowWidth(double width) {
        app.stage().setWidth(width);
    }
    
    public void setWindowHeight(double height) {
        app.stage().setHeight(height);
    }
    
    public void setWindowMaximized(boolean isMaximized) {
        app.stage().setMaximized(isMaximized);
    }
    
    // METHODS
    
    
}
