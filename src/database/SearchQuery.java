package database;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonObject;

/**
 * The <code>SearchQuery</code> class contains data pertaining to an OMDb search
 *      request and the SearchResults found.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 21, 2016
 */

public class SearchQuery {
    // JSON CONSTANTS
    private final String
            JSON_SEARCH_RESULTS = "Search",
            JSON_TOTAL_RESULTS  = "totalResults",
            JSON_RESPONSE       = "Response",
            JSON_ERROR          = "Error";
    
    // ATTRIBUTES
    private ObservableList<SearchResult> results;
    private int totalResults;
    private boolean response;
    private String error;
    
    // CONSTRUCTORS
    public SearchQuery() {
        results = FXCollections.observableArrayList();
    }
    
    // ACCESSORS
    public ObservableList<SearchResult> getResults() { return results; }
    public int getTotalResults()                { return totalResults; }
    public boolean getResponse()                { return response; }
    public String getError()                    { return error; }
    
    // MUTATORS
    public void setResults(ObservableList<SearchResult> results) { this.results = results; }
    public void setTotalResults(int totalResults)           { this.totalResults = totalResults; }
    public void setResponse(boolean response)               { this.response = response; }
    public void setError(String error)                      { this.error = error; }
    
    // METHODS
    public void fromJSON(JsonObject jso) {
        // CLEAR RESULTS ARRAY
        results.clear();
        
        // GET RESPONSE
        response = stringToBool(jso.getString(JSON_RESPONSE));
        
        if(!response) {
            
            error = jso.getString(JSON_ERROR);
            
        } else {
        
            JsonArray resultsArray = jso.getJsonArray(JSON_SEARCH_RESULTS);

            for(int i = 0; i < resultsArray.size(); i++) {
                SearchResult result = new SearchResult();
                result.fromJSON(resultsArray.getJsonObject(i));
                
                result.setIndex(i + 1);

                results.add(result);
            }

            totalResults = Integer.parseInt(jso.getString(JSON_TOTAL_RESULTS));
        }
    }
    
    private boolean stringToBool(String str) {
        return str.toLowerCase().equals("true");
    }
}
