package database;

/**
 * The <code>OMDbBuilder</code> is 
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Jul 28, 2016
 *
 */
public class OMDbBuilder {
    // ATTRIBUTES
    private String title;
    private final String type = "movie";
    private String year, id;
    private final String plot = "full";

    // CONSTRUCTORS
    public OMDbBuilder(String title) {
        this.title = stringReplaceSpaces(title);
        this.id = "";
        this.year = "";
    }

    public OMDbBuilder(String title, String year) {
        this.title = stringReplaceSpaces(title);
        this.year = year;
    }

    // ACCESSORS
    public String getTitle() {
        return title;
    }

    public String getID() {
        return id;
    }

    public String getYear() {
        return year;
    }

    // MUTATORS
    public void setTitle(String title) {
        this.title = stringReplaceSpaces(title);
    }

    public void setID(String id) {
        this.id = id;
    }

    public void setYear(String year) {
        this.year = year;
    }

    // METHODS
    public String search() throws Exception {

        String[] parameters = {"s", "type", "y"};
        String[] values = {title, type, year};

        return OMDb.sendGet(parameters, values);
    }

    public String byTitle() throws Exception {

        String[] parameters = {"t", "type", "y", "plot"};
        String[] values = {title, type, year, plot};

        return OMDb.sendGet(parameters, values);
    }

    public String byID() throws Exception {

        String[] parameters = {"i", "type", "y", "plot"};
        String[] values = {id, type, year, plot};

        return OMDb.sendGet(parameters, values);
    }

    // PRIVATE HELPER METHODS

    // Private helper that replaces spaces with a plus sign
    private String stringReplaceSpaces(String string) {
        String output = "";

        for(char c : string.toCharArray()) {
            if(c == ' ') {
                output += "+";
            } else {
                output += c;
            }
        }

        return output;
    }
}
