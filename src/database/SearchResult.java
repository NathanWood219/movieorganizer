package database;

import javax.json.JsonObject;

/**
 * The <code>SearchResult</code> class allows the easy parsing of a single
 *      result in the form of JSON data from an OMDb search query.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 21, 2016
 */

public class SearchResult {
    // JSON CONSTANTS
    private final String
            JSON_TITLE      = "Title",
            JSON_YEAR       = "Year",
            JSON_IMDBID     = "imdbID",
            JSON_TYPE       = "Type",
            JSON_POSTER     = "Poster";
    
    // ATTRIBUTES
    private int index;
    private String title, year, IMDBid, type, poster;
    
    // CONSTRUCTORS
    public SearchResult() {
        index = -1;
    }
    
    // ACCESSORS
    public int getIndex()       { return index; }
    public String getTitle()    { return title; }
    public String getYear()     { return year; }
    public String getIMDBid()   { return IMDBid; }
    public String getType()     { return type; }
    public String getPoster()   { return poster; }
    
    // MUTATORS
    public void setIndex(int index)         { this.index = index; }
    public void setTitle(String title)      { this.title = title; }
    public void setYear(String year)        { this.year = year; }
    public void setIMDBid(String IMDBid)    { this.IMDBid = IMDBid; }
    public void setType(String type)        { this.type = type; }
    public void setPoster(String poster)    { this.poster = poster; }
    
    // METHODS
    public void fromJSON(JsonObject jso) {
        title   = jso.getString(JSON_TITLE);
        year    = jso.getString(JSON_YEAR);
        IMDBid  = jso.getString(JSON_IMDBID);
        type    = jso.getString(JSON_TYPE);
        poster  = jso.getString(JSON_POSTER);
    }
}
