package database;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * The <code>OMDb</code> class accesses the IMDb using the Open Movie Database API 
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Jul 24, 2016
 *
 */

public class OMDb {
    // ATTRIBUTES

    private static final String USER_AGENT = "Mozilla/5.0";

    // PUBLIC METHODS

    // HTTP GET request
    public static String sendGet(String[] parameters, String[] values) throws Exception {

        String url = "http://www.omdbapi.com/?" + parameters[0] + "=" + values[0];

        for(int i = 1; i < parameters.length; i++) {
            if(!values[i].isEmpty()) {
                url += "&" + parameters[i] + "=" + values[i];
            }
        }

        // SET REQUEST TO JSON FORMAT
        url += "&r=json";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        // add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

        //int responseCode = con.getResponseCode();

        //System.out.println("\nSending 'GET' request to URL : " + url);
        //System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // RETURN RESULT
        return response.toString();

    }
}
