package data;

import java.util.Collections;

import application.MainApplication;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * The <code>DataManager</code> class handles all the data storage and access for the MovieOrganizer program.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 1, 2016
 *
 */
public class DataManager {
    // ATTRIBUTES
    MainApplication app;
    
    // MOVIES DATA
    ObservableList<Movie> moviesArray;
    ObservableList<Movie> newMovies;
    ObservableList<Movie> moviesSearchArray;
    ObservableList<Movie> garbage;

    // ERRORS DATA
    ObservableList<Movie> errors;

    // CONSTRUCTORS
    public DataManager(MainApplication app) {
        this.app = app;
        
        moviesArray         = FXCollections.observableArrayList();
        newMovies           = FXCollections.observableArrayList();
        moviesSearchArray   = FXCollections.observableArrayList();
        garbage             = FXCollections.observableArrayList();
        errors              = FXCollections.observableArrayList();

        copyResults(moviesArray);
    }

    // ACCESSORS
    public ObservableList<Movie> getMovies() {
        return moviesArray;
    }
    
    public ObservableList<Movie> getNewMovies() {
        return newMovies;
    }

    public ObservableList<Movie> getSearchResults() {
        return moviesSearchArray;
    }
    
    public ObservableList<Movie> getGarbage() {
        return garbage;
    }
    
    public ObservableList<Movie> getErrors() {
        return errors;
    }

    // MUTATORS
    public void addMovie(Movie movie) {
        moviesArray.add(movie);
    }
    
    public void removeMovie(Movie movie) {
        moviesArray.remove(movie);
    }

    public void addResult(Movie movie) {
        moviesSearchArray.add(movie);
    }
    
    public void addGarbage(Movie movie) {
        garbage.add(movie);
    }
    
    public void removeGarbage(Movie movie) {
        garbage.remove(movie);
    }
    
    public void addError(Movie error) {
        errors.add(error);
    }
    
    public void removeError(Movie error) {
        errors.remove(error);
    }

    // METHODS

    public void sortMovies() {
        Collections.sort(moviesArray);
    }

    public void startSearch() {
        moviesSearchArray = FXCollections.observableArrayList();
    }
    
    public boolean hasError(Movie error) {
        for(Movie movie : errors) {
            if(movie.getFilename().equals(error.getFilename())) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean hasGarbage(Movie garbageMovie) {
        for(Movie movie : garbage) {
            if(movie.getFilename().equals(garbageMovie.getFilename())) {
                return true;
            }
        }
        
        return false;
    }

    public void copyMovies(ObservableList<Movie> movies) {
        moviesArray.clear();

        for(Movie movie : movies) {
            moviesArray.add(movie.clone());
        }
    }

    public void copyNewMovies(ObservableList<Movie> movies) {
        newMovies.clear();

        for(Movie movie : movies) {
            newMovies.add(movie.clone());
        }
    }

    public void copyResults(ObservableList<Movie> movies) {
        moviesSearchArray.clear();

        for(Movie movie : movies) {
            moviesSearchArray.add(movie.clone());
        }
    }
    
    public void copyGarbage(ObservableList<Movie> newGarbage) {
        garbage.clear();
        
        for(Movie movie : newGarbage) {
            garbage.add(movie.clone());
        }
    }
    
    public void copyErrors(ObservableList<Movie> newErrors) {
        errors.clear();
        
        for(Movie error : newErrors) {
            errors.add(error.clone());
        }
    }
}
