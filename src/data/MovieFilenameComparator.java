package data;

import java.util.Comparator;

/**
 * The <code>MovieFilenameComparator</code> allows the movie to be sorted by filename instead of title.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 3, 2016
 *
 */
public class MovieFilenameComparator implements Comparator<Movie> {

    @Override
    public int compare(Movie m1, Movie m2) {

            return m1.getFilename().compareTo(m2.getFilename());
    }
}
