package data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.json.Json;
import javax.json.JsonObject;

/*      THIS CLASS IS DEPRECATED       */

/**
 * The <code>Error</code> class contains data for a movie file that failed
 *  to match with any data from the online database.
 * 
 *      DEPRECATED - Movie class contains same data
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 11, 2016
 */
public class Error {
    // JSON CONSTANTS
    private final String
            JSON_FILENAME   = "Filename",
            JSON_FILEPATH   = "Filepath";
    
    // ATTRIBUTES
    private final StringProperty filename, filepath;
    
    // CONSTRUCTORS
    public Error() {
        filename = new SimpleStringProperty();
        filepath = new SimpleStringProperty();
    }
    
    public Error(String filename, String filepath) {
        this.filename = new SimpleStringProperty(filename);
        this.filepath = new SimpleStringProperty(filepath);
    }
    
    // ACCESSORS
    public String getFilename() { return filename.get(); }
    
    public String getFilepath() { return filepath.get(); }
    
    // MUTATORS
    public void setFilename(String filename) { this.filename.set(filename); }
    
    public void setFilepath(String filepath) { this.filepath.set(filepath); }
    
    // METHODS
    public JsonObject toJSON() {
        return Json.createObjectBuilder()
                .add(JSON_FILENAME, filename.get())
                .add(JSON_FILEPATH, filepath.get())
                .build();
    }
    
    public void fromJSON(JsonObject jso) {
        filename.set(jso.getString(JSON_FILENAME));
        filepath.set(jso.getString(JSON_FILEPATH));
    }
    
    @Override
    public Error clone() {
        Error error = new Error();
        
        error.filename.set(filename.get());
        error.filepath.set(filename.get());
        
        return error;
    }
    
}
