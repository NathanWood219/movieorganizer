package data;

import javax.json.Json;
import javax.json.JsonObject;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import strings.StringParser;

/**
 * The <code>Movie</code> class contains all the data for each movie file,
 * 	as well as ways to convert to/from JSON format.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Jul 28, 2016
 *
 */
public class Movie implements Comparable<Movie> {
    // JSON CONSTANTS
    private final String
            JSON_TITLE 		= "Title",
            JSON_YEAR		= "Year",
            JSON_RATING		= "Rated",
            JSON_RUNTIME	= "Runtime",
            JSON_GENRES		= "Genre",
            JSON_DIRECTORS	= "Director",
            JSON_WRITERS	= "Writer",
            JSON_ACTORS		= "Actors",
            JSON_PLOT		= "Plot",
            JSON_LANGUAGE	= "Language",
            JSON_COUNTRY	= "Country",
            JSON_POSTER		= "Poster",
            JSON_METASCORE	= "Metascore",
            JSON_IMDBRATING	= "imdbRating",
            JSON_IMDBVOTES	= "imdbVotes",
            JSON_IMDBID		= "imdbID",
            JSON_FILENAME       = "Filename",
            JSON_FILEPATH       = "Filepath";


    // ATTRIBUTES
    private final StringProperty title, year, rating, runtime, genres, directors, writers, actors,
            plot, languages, countries, poster, metascore, IMDBRating, IMDBVotes, IMDBid, filename, filepath;

    // CONSTRUCTORS
    public Movie() {
        title		= new SimpleStringProperty();
        year		= new SimpleStringProperty();
        rating		= new SimpleStringProperty();
        runtime		= new SimpleStringProperty();
        genres		= new SimpleStringProperty();
        directors	= new SimpleStringProperty();
        writers		= new SimpleStringProperty();
        actors		= new SimpleStringProperty();
        plot		= new SimpleStringProperty();
        languages	= new SimpleStringProperty();
        countries	= new SimpleStringProperty();
        poster		= new SimpleStringProperty();
        metascore	= new SimpleStringProperty();
        IMDBRating	= new SimpleStringProperty();
        IMDBVotes	= new SimpleStringProperty();
        IMDBid		= new SimpleStringProperty();
        filename        = new SimpleStringProperty();
        filepath	= new SimpleStringProperty();
    }

    // ACCESSORS
    public String getTitle() { return title.get(); }

    public String getYear() { return year.get(); }

    public String getRating() { return rating.get(); }

    public String getRuntime() { return runtime.get(); }

    public String getGenres() { return genres.get(); }

    public String getDirectors() { return directors.get(); }

    public String getWriters() { return writers.get(); }

    public String getActors() { return actors.get(); }

    public String getPlot() { return plot.get(); }

    public String getLanguages() { return languages.get(); }

    public String getCountries() { return countries.get(); }

    public String getPoster() { return poster.get(); }

    public String getMetascore() { return metascore.get(); }

    public String getIMDBRating() { return IMDBRating.get(); }

    public String getIMDBVotes() { return IMDBVotes.get(); }

    public String getIMDBid() { return IMDBid.get(); }

    public String getFilename() { return filename.get(); }

    public String getFilepath() { return filepath.get(); }

    // MUTATORS
    public void setTitle(String title) { this.title.set(title); }

    public void setYear(String year) { this.year.set(year); }

    public void setRating(String rating) { this.rating.set(rating); }

    public void setRuntime(String runtime) { this.runtime.set(runtime); }

    public void setGenres(String genres) { this.genres.set(genres); }

    public void setDirectors(String directors) { this.directors.set(directors); }

    public void setWriters(String writers) { this.writers.set(writers); }

    public void setActors(String actors) { this.actors.set(actors); }

    public void setPlot(String plot) { this.plot.set(plot); }

    public void setLanguages(String languages) { this.languages.set(languages); }

    public void setCountries(String countries) { this.countries.set(countries); }

    public void setPoster(String poster) { this.poster.set(poster); }

    public void setMetascore(String metascore) { this.metascore.set(metascore); }

    public void setIMDBRating(String IMDBRating) { this.IMDBRating.set(IMDBRating); }

    public void setIMDBVotes(String IMDBVotes) { this.IMDBVotes.set(IMDBVotes); }

    public void setIMDBid(String IMDBid) { this.IMDBid.set(IMDBid); }

    public void setFilename(String filename) { this.filename.set(filename); }

    public void setFilepath(String filepath) { this.filepath.set(filepath); }

    // METHODS

    public boolean containsPhrase(String phrase) {
        phrase = phrase.toLowerCase();

        return StringParser.containsFull(title.get().toLowerCase(), phrase) || StringParser.containsFull(year.get().toLowerCase(), phrase)
            || StringParser.containsFull(rating.get().toLowerCase(), phrase) || StringParser.containsFull(genres.get().toLowerCase(), phrase) 
            || StringParser.containsFull(directors.get().toLowerCase(), phrase) || StringParser.containsFull(writers.get().toLowerCase(), phrase)
            || StringParser.containsFull(actors.get().toLowerCase(), phrase) || StringParser.containsFull(languages.get().toLowerCase(), phrase)
            || StringParser.containsFull(countries.get().toLowerCase(), phrase);
    }

    public JsonObject toMovieJSON() {
        return Json.createObjectBuilder()
            .add(JSON_TITLE,		title.get())
            .add(JSON_YEAR,		year.get())
            .add(JSON_RATING,		rating.get())
            .add(JSON_RUNTIME,		runtime.get())
            .add(JSON_GENRES,		genres.get())
            .add(JSON_DIRECTORS,	directors.get())
            .add(JSON_WRITERS,		writers.get())
            .add(JSON_ACTORS,		actors.get())
            .add(JSON_PLOT,		plot.get())
            .add(JSON_LANGUAGE,		languages.get())
            .add(JSON_COUNTRY,		countries.get())
            .add(JSON_POSTER,		poster.get())
            .add(JSON_METASCORE,	metascore.get())
            .add(JSON_IMDBRATING,	IMDBRating.get())
            .add(JSON_IMDBVOTES,	IMDBVotes.get())
            .add(JSON_IMDBID,		IMDBid.get())
            .add(JSON_FILENAME,         filename.get())
            .add(JSON_FILEPATH,		filepath.get())
            .build();
    }
    
    public JsonObject toFileJSON() {
        return Json.createObjectBuilder()
            .add(JSON_TITLE,		title.get())
            .add(JSON_YEAR,		year.get())
            .add(JSON_FILENAME,         filename.get())
            .add(JSON_FILEPATH,		filepath.get())
            .build();
    }

    public void fromMovieJSON(JsonObject jso, boolean includeFile) {
        title.set(	jso.getString(JSON_TITLE));
        year.set(	jso.getString(JSON_YEAR	));
        rating.set(	jso.getString(JSON_RATING));

        // CHANGE FORMAT OF RUNTIME FROM "123 min" TO "2h 3m"
        runtime.set(	jso.getString(JSON_RUNTIME));
        
        genres.set(	jso.getString(JSON_GENRES));
        
        // REMOVE BACKSLASHES
        directors.set(	StringParser.removeBackslashes(jso.getString(JSON_DIRECTORS)));
        writers.set(	StringParser.removeBackslashes(jso.getString(JSON_WRITERS)));
        actors.set(	StringParser.removeBackslashes(jso.getString(JSON_ACTORS)));
        plot.set(	StringParser.removeBackslashes(jso.getString(JSON_PLOT)));

        languages.set(	jso.getString(JSON_LANGUAGE));
        countries.set(	jso.getString(JSON_COUNTRY));
        poster.set(	jso.getString(JSON_POSTER));
        metascore.set(	jso.getString(JSON_METASCORE));
        IMDBRating.set(	jso.getString(JSON_IMDBRATING));
        IMDBVotes.set(	jso.getString(JSON_IMDBVOTES));
        IMDBid.set(	jso.getString(JSON_IMDBID));

        // WHEN PULLING FROM IMDB, FILE DATA ISN'T INCLUDED (ADDED TO SAVE FILE THOUGH)
        if(includeFile) {
            fromFileJSON(jso);
        }
    }
    
    public void fromFileJSON(JsonObject jso) {
        title.set(	jso.getString(JSON_TITLE));
        year.set(	jso.getString(JSON_YEAR	));
        filename.set(   jso.getString(JSON_FILENAME));
        filepath.set(   jso.getString(JSON_FILEPATH));
    }

    @Override
    public int compareTo(Movie movie) {
        return title.get().compareTo(movie.getTitle());
    }

    @Override
    public Movie clone() {
        Movie newMovie = new Movie();

        newMovie.title.set(	title.get());
        newMovie.year.set(	year.get());
        newMovie.rating.set(	rating.get());
        newMovie.runtime.set(	runtime.get());
        newMovie.genres.set(	genres.get());
        newMovie.directors.set(	directors.get());
        newMovie.writers.set(	writers.get());
        newMovie.actors.set(	actors.get());
        newMovie.plot.set(	plot.get());
        newMovie.languages.set(	languages.get());
        newMovie.countries.set(	countries.get());
        newMovie.poster.set(	poster.get());
        newMovie.metascore.set(	metascore.get());
        newMovie.IMDBRating.set(IMDBRating.get());
        newMovie.IMDBVotes.set(	IMDBVotes.get());
        newMovie.IMDBid.set(	IMDBid.get());
        newMovie.filename.set(  filename.get());
        newMovie.filepath.set(	filepath.get());

        return newMovie;
    }

    public String toTableString() {		
        return String.format("%-25s | %4s | %9s | %7s | %-30s | %-30s | %-30s | %-60s |",
            StringParser.limitString(title.get(), 25), year.get(), StringParser.centerString(rating.get(), 9), runtime.get(),
            StringParser.limitString(genres.get(), 30), StringParser.limitString(directors.get(), 30),
            StringParser.limitString(writers.get(), 30), StringParser.limitString(actors.get(), 60));
    }

    @Override
    public String toString() {
        return "Title: " + title.get() + " (" + year.get() + "); Rated: "
                + rating.get() + ", Runtime: " + runtime.get() + "\nPlot: " + plot.get();
    }

}