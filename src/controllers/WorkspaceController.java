package controllers;

import application.MainApplication;
import data.Movie;
import gui.TabBuilder;
import gui.WindowBuilder;
import gui.Workspace;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Tab;
import javafx.stage.DirectoryChooser;
import settings.SettingsManager;
import strings.StringParser;
import tasks.ScanTask;
import tasks.SearchTask;
import updater.DataRequest;
import updater.UpdateLoader;

/**
 * The <code>WorkspaceController</code> handles events triggered by components in the Workspace class.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 2, 2016
 *
 */
public class WorkspaceController {
    // ATTRIBUTES
    MainApplication app;

    // CONSTRUCTORS
    public WorkspaceController(MainApplication app) {
        this.app = app;
    }

    // METHODS
    
    public void processOpenMovieTab(Movie movie) {
        if(movie != null) {
            Tab tab = findTab(movie.getTitle());

            if(tab == null) {
                TabBuilder tabBuilder = new TabBuilder(app);
                app.getWorkspaceComponent().addTab(tabBuilder.buildMovieTab(movie));
            } else {
                app.getWorkspaceComponent().selectTab(tab);
            }
        }
    }
    
    public void processOpenErrorsList() {
        Tab tab = findTab("Errors");
        
        if(tab == null) {
            TabBuilder tabBuilder = new TabBuilder(app);
            app.getWorkspaceComponent().addTab(tabBuilder.buildErrorsTab());
        } else {
            app.getWorkspaceComponent().selectTab(tab);
        }
    }
    
    public void processOpenGarbageList() {
        Tab tab = findTab("Garbage");
        
        if(tab == null) {
            TabBuilder tabBuilder = new TabBuilder(app);
            app.getWorkspaceComponent().addTab(tabBuilder.buildGarbageTab());
        } else {
            app.getWorkspaceComponent().selectTab(tab);
        }
    }

    @SuppressWarnings("unchecked")
    public void	processScanMovies(boolean scanNestedFolders) {

        app.trace("Scanning movies...");

        app.getWorkspaceComponent().scanStarted();

        ScanTask scanTask = new ScanTask(app, scanNestedFolders);
        new Thread(scanTask).start();

        // BIND PROGRESS TO WORKSPACE PROGRESSBAR
        bindProgress(scanTask);

        // SET HANDLER TO NOTIFY WHEN TASK IS COMPLETE
        scanTask.setOnSucceeded(e -> {
            app.getWorkspaceComponent().scanFinished();
            app.trace("ScanTask completed.");
            
            WindowBuilder.buildScanReportWindow(app, scanTask);
            
            // OPEN NEW MOVIES LIST OR ERRORS LIST
            if(scanTask.getErrorCount() > 0) {
                processOpenErrorsList();
            }
            
            if(scanTask.getNewMovieCount() > 0) {
                app.getWorkspaceComponent().addMoviesTable(app.getDataComponent().getNewMovies(), false);
            }
            
            unbindProgress();
        });

        // SET HANDLER TO NOTIFY IF TASK FAILS
        scanTask.setOnFailed(e -> {
            app.getWorkspaceComponent().scanFinished();
            app.trace("ERROR: ScanTask failed.");
            
            WindowBuilder.buildBugWindow(app, "An error occurred while trying to scan for movies.");

            unbindProgress();
        });

        // SET HANDLER TO NOTIFY IF TASK IS CANCELLED
        scanTask.setOnCancelled(e -> {
            app.getWorkspaceComponent().scanFinished();
            app.trace("ScanTask was cancelled.");

            unbindProgress();
        });
    }

    public void processChangeDirectory() {

        app.trace("Changing directory...");
        
        SettingsManager settingsManager = app.getSettingsComponent();
        
        DirectoryChooser dirChooser = new DirectoryChooser();
        
        dirChooser.setInitialDirectory(new File(settingsManager.getMoviesPath()));
        dirChooser.setTitle("Select a Movies Directory");
        
        File moviesFolder = dirChooser.showDialog(app.stage());
        
        if(moviesFolder == null) {
            app.trace("No folder was selected (action was cancelled).");
        } else {
            settingsManager.setMoviesPath(moviesFolder.getAbsolutePath());
            app.trace("New movies directory: " + moviesFolder.getAbsolutePath());
        }

    }
    
    public void processUpdate(String link, boolean showNoUpdates) {
        app.trace("Looking for updates...");
        
        try {
            UpdateLoader loader = new UpdateLoader(DataRequest.pull(link));
            
            app.trace("\n" + loader.toString() + "\n");
            
            if(loader.getVersion() > app.getVersion()) {
                app.trace("Current version: " + app.getVersion() + "; latest version: " + loader.getVersion());
                WindowBuilder.buildUpdatesWindow(app);
            } else {
                app.trace("Caboodle is up to date.");
                
                if(showNoUpdates) {
                    WindowBuilder.buildOkWindow(app, "Updates", "Caboodle is up to date!");
                }
            }
            
        } catch(Exception e) {
            app.traceEx(e);
            WindowBuilder.buildBugWindow(app, "An error occurred while trying to check for updates.");
        }
    }

    public void processExit() {
        try {
            app.trace("\nAttempting to save settings on exit...");
            
            // GRAB VIEW SETTINGS
            app.getSettingsComponent().setViewSettings(app.getWorkspaceComponent().getViewSettings());
            
            app.getFileComponent().saveSettings(app.getSettingsComponent(), app.getWorkspaceComponent().getSettingsPath());
            app.trace("Settings successfully saved.");
        } catch(IOException e) {
            app.trace("ERROR: Unable to save settings.");
            app.traceEx(e);
        }
        
        app.trace("Exiting application gracefully...");
        System.exit(0);
    }

    @SuppressWarnings("unchecked")
    public void processSearch(String searchQuery) {
        app.trace("Searching for '" + searchQuery + "'...");

        app.getDataComponent().startSearch();

        Workspace workspace = app.getWorkspaceComponent();
        workspace.searchStarted();

        SearchTask searchTask = new SearchTask(app, searchQuery);
        new Thread(searchTask).start();

        // SET HANDLER TO NOTIFY WHEN TASK IS COMPLETE
        searchTask.setOnSucceeded(e -> {
            app.trace("SearchTask successfully finished.");
            workspace.searchFinished();
            
            TabBuilder tabBuilder = new TabBuilder(app);
            
            int resultsCount = app.getDataComponent().getSearchResults().size();
            
            if(resultsCount == 1) {
                app.trace("Only one result found, opening movie tab.");
                workspace.addTabFromMovie(app.getDataComponent().getSearchResults().get(0));
            } else {
                app.trace("Search found " + resultsCount + " result(s). Opening search tab.");
                workspace.addTab(tabBuilder.buildSearchTab(searchQuery));
            }
        });
        
        searchTask.setOnFailed(e -> {
            app.trace("ERROR: SearchTask failed!");
            WindowBuilder.buildBugWindow(app, "An error occurred while trying to search.");
        });
    }
    
    public void processDownloadPoster(Movie movie, boolean overwrite) {
        // SAVE COVER TO SPECIFIED FOLDER
        try {
            if(!movie.getPoster().equals("N/A")) {
                String fullFilepath = app.getWorkspaceComponent().getPostersPath() + "\\"
                        + StringParser.removeExtension(movie.getFilename())
                        + "." + StringParser.getExtension(movie.getPoster());

                if(!new File(fullFilepath).exists() || overwrite) {
                    saveImage(movie.getPoster(), fullFilepath);
                    app.trace("Successfully downloaded poster.");
                }
            }

        } catch(Exception e) {
            app.trace("Failed to download poster.");
            app.traceEx(e);
        }
    }
    
    // HELPER METHODS
    
    private Tab findTab(String tabName) {
        for(Tab tab : app.getWorkspaceComponent().getTabs()) {
            if(tab.getText().equals(tabName)) {
                return tab;
            }
        }
        
        return null;
    }
    
    private void saveImage(String imageUrl, String destinationFile) throws IOException {
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(destinationFile);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    private void bindProgress(ScanTask task) {
        ProgressBar progressBar = app.getWorkspaceComponent().getProgressBar();

        progressBar.setProgress(0);
        progressBar.progressProperty().bind(task.getPercentageDone());
        progressBar.setVisible(true);
        
        Label progressLabel = app.getWorkspaceComponent().getProgressLabel();
        progressLabel.setVisible(true);
    }

    private void unbindProgress() {
        ProgressBar progressBar = app.getWorkspaceComponent().getProgressBar();

        progressBar.progressProperty().unbind();
        progressBar.setProgress(0);
        progressBar.setVisible(false);
        
        Label progressLabel = app.getWorkspaceComponent().getProgressLabel();
        progressLabel.setVisible(false);
    }

}
