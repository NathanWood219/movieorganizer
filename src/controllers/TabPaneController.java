package controllers;

import application.MainApplication;
import data.Movie;
import gui.WindowBuilder;
import gui.Workspace;
import java.awt.Desktop;
import java.io.File;
import java.util.ArrayList;
import javafx.scene.control.Tab;

/**
 *
 * @author Nathan
 */
public class TabPaneController {
    // ATTRIBUTES
    MainApplication app;
    
    // CONSTRUCTORS
    public TabPaneController(MainApplication app) {
        this.app = app;
    }

    // METHODS
    
    public void processPlayMovie(Movie movie) {
        try {
            app.trace("Attempting to open '" + movie.getFilename() + "'...");

            Desktop.getDesktop().open(new File(movie.getFilepath() + "\\" + movie.getFilename()));

            app.trace("Successfully opened the movie.");
        } catch(Exception ex) {
            app.trace("Failed to open movie!");
            app.traceEx(ex);
        }
    }
    
    public void processCorrectData(Movie movie, boolean isError) {
        app.trace("Opening correct movie window for '" + movie.getFilename() + "'");
        WindowBuilder.buildCorrectMovieWindow(app, movie, isError);
    }
    
    public void processCloseTab(Tab clickedTab) {
        Workspace workspace = app.getWorkspaceComponent();
        workspace.getTabs().remove(clickedTab);
    }
    
    public void processCloseAllTabs() {
        Workspace workspace = app.getWorkspaceComponent();
        
        // SELECT THE TAB TO LEAVE OPEN (MOVIE TAB)
        workspace.selectTab(workspace.getMoviesTab());

        ArrayList<Tab> removedTabs = new ArrayList<>();

        // GET TABS TO REMOVE
        for(Tab tab : workspace.getTabs()) {
            if(tab.isClosable()) {
                removedTabs.add(tab);
            }
        }

        // REMOVE THE TABS
        for(Tab tab : removedTabs) {
            workspace.getTabs().remove(tab);
        }
    }
    
    public void processCloseOtherTabs(Tab clickedTab) {
        Workspace workspace = app.getWorkspaceComponent();
        
        // SELECT THE TAB TO LEAVE OPEN
        workspace.selectTab(clickedTab);

        ArrayList<Tab> removedTabs = new ArrayList<>();

        // GET TABS TO REMOVE
        for(Tab tab : workspace.getTabs()) {
            if(!tab.equals(clickedTab) && tab.isClosable()) {
                removedTabs.add(tab);
            }
        }

        // REMOVE THE TABS
        for(Tab tab : removedTabs) {
            workspace.getTabs().remove(tab);
        }
    }
}
