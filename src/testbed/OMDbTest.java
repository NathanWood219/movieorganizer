package testbed;

import database.OMDbBuilder;

/**
 * The <code>OMDbTest</code> is 
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Jul 24, 2016
 *
 */
public class OMDbTest {
	
	public static void main(String[] args) {
		
		try {
			OMDbBuilder movie = new OMDbBuilder("Harry Potter 4 And the Prisoner of Azkaban", "2004");
			
			System.out.println(movie.byTitle());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
