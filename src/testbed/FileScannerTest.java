package testbed;

import java.io.File;
import localfiles.FileScanner;

/**
 * The <code>FileScannerTest.java</code> is 
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Jul 29, 2016
 *
 */
public class FileScannerTest {
	public static void main(String[] args) {		
		System.out.print("Using the movie directory: 'E:\\1. Movies'\n");
		String dirPath = "E:\\1. Movies";
		
		String[] filenames = FileScanner.findFilenames(new File(dirPath));
		
		for(int i = 0; i < filenames.length; i++) {
			System.out.println("\t" + (i + 1) + "\t" + filenames[i]);
		}
	}

}
