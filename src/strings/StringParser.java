package strings;

/**
 * The <code>StringParser</code> is a library of static methods that
 * 	take in Strings as arguments and return information based on them.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 3, 2016
 *
 */
public class StringParser {
    // METHODS

    public static boolean isDigits(String text) {        
        for(int i = 0; i < text.length(); i++) {
            if(text.charAt(i) < '0' || text.charAt(i) > '9') {
                return false;
            }
        }

        return true;
    }
    
    public static String removeBackslashes(String str) {
    	String output = "";
    	
    	for(int i = 0; i < str.length(); i++) {
            if(str.charAt(i) != '\\') {
                output += str.charAt(i);
            }
    	}
    	
    	return output;
    }
    
    public static String changeRuntimeFormat(String runtime) {

        // IF THIS IS TRUE, THEN THE RUNTIME HAS ALREADY BEEN CORRECTED
        if(!runtime.contains("min")) {
            return runtime;
        }

        int hours, minutes = 0;

        // SOME RUNTIMES ARE EQUAL TO "N/A" OR THE LIKES
        if(!isDigits(runtime.substring(0, 1))) {
            return runtime;
        }

        // FIND THE FULL RUNTIME IN MINUTES
        for(int i = 2; i < runtime.length(); i++) {
            if(!isDigits(runtime.substring(0, i))) {
                minutes = Integer.parseInt(runtime.substring(0, i - 1));
                break;
            }
        }

        // PULL OUT THE NUMBER OF HOURS
        hours = minutes / 60;
        minutes = minutes % 60;

        // IF 0 HOURS, DO NOT RETURN THE HOURS PORTION OF THE STRING
        if(hours == 0) {
            return minutes + "m";
        } else {
            return hours + "h " + minutes + "m";
        }
    }
    
    public static String filepathToFilename(String filepath) {
    	for(int i = filepath.length() - 1; i >= 0; i--) {    		
            if(filepath.charAt(i) == '\\') {
                return filepath.substring(i + 1);
            }
    	}
    	
    	return "";
    }
    
    public static int getRuntime(String runtime) {
        if(runtime.contains("min")) {
            for(int i = 2; i < runtime.length(); i++) {
                if(!isDigits(runtime.substring(0, i))) {
                    return Integer.parseInt(runtime.substring(0, i - 1));
                }
            }
        }
        
        return -1;
    }
    
    public static String limitString(String str, int maxLength) {
    	if(str.length() > maxLength) {
            return str.substring(0, maxLength - 3) + "...";
    	}
    	
    	return str;
    }
    
    public static String centerString(String str, int length) {
    	String output = str;
    	
    	while(output.length() < length) {
            output += " ";

            if(output.length() < length) {
                output = " " + output;
            }
    	}
    	
    	return output;
    }
    
    /**
     *	Checks if the phrase is contained completely (either at the start of
     * 		the string, at the end, and/or has spaces on either side)
     * 
     * @param string
     * 	The string to search
     * @param phrase
     * 	The phrase to search the string for
     * @return
     * 	Returns true if the phrase is completely contained
     */
    public static boolean containsFull(String string, String phrase) {
    	int index = string.indexOf(phrase);
    	
    	if(index == -1) {
            return false;
    	}
    	
    	boolean validLeft = false, validRight = false;
    	
    	if(index == 0 || isSplitterSymbol(string.charAt(index - 1))) {
            validLeft = true;
    	}
    	
    	if(index + phrase.length() == string.length() || isSplitterSymbol(string.charAt(index + phrase.length()))) {
            validRight = true;
    	}
    	
    	return validLeft && validRight;
    }
    
    // USED BY containsFull() TO VERIFY SYMBOL
    public static boolean isSplitterSymbol(char symbol) {
    	return (symbol == ' ' || symbol == ',' || symbol == '.' || symbol == '?'
                || symbol == '!' || symbol == ':' || symbol == ';');
    }
	
    public static String getExtension(String string) {
        return string.substring(string.lastIndexOf(".") + 1);
    }
	
    public static String removeExtension(String string) {
        return string.substring(0, string.lastIndexOf("."));
    }

}
