package application;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import localfiles.FileManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import data.DataManager;
import gui.Workspace;
import java.awt.Desktop;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import settings.SettingsManager;

/**
 * The <code>MainApplication</code> launches the JavaFX gui for the MovieOrganizer application.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 1, 2016
 *
 */
public class MainApplication extends Application {
    // CONSTANTS
    final int
        WINDOW_MINWIDTH		= 900,
        WINDOW_MINHEIGHT	= 600;
    
    final int VERSION = 1;

    // ATTRIBUTES
    private FileManager fileManager;
    private DataManager dataManager;
    private SettingsManager settingsManager;
    private Workspace workspace;

    private final boolean DEBUGGING = true;
    
    private String log;

    private Stage stage;
    private Scene scene;

    // LAUNCH THE JAVAFX APPLICATION
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        this.scene = new Scene(new VBox(), WINDOW_MINWIDTH, WINDOW_MINHEIGHT);
        
        this.log = "";

        // SET INITIAL PROPERTIES OF THE APPLIATION
        stage.setTitle("Caboodle");
        stage.setMinWidth(WINDOW_MINWIDTH);
        stage.setMinHeight(WINDOW_MINHEIGHT);
        stage.setScene(scene);
        stage.show();

        // CONSTRUCT EACH COMPONENT
        fileManager = new FileManager();
        dataManager = new DataManager(MainApplication.this);
        settingsManager = new SettingsManager(MainApplication.this);        
        workspace = new Workspace(MainApplication.this);
    }

    public static void main(String[] args) {
        launch(args);
    }

    // ACCESSORS
    public FileManager getFileComponent() {
        return fileManager;
    }

    public DataManager getDataComponent() {
        return dataManager;
    }
    
    public SettingsManager getSettingsComponent() {
        return settingsManager;
    }

    public Workspace getWorkspaceComponent() {
        return workspace;
    }

    public Stage stage() {
        return stage;
    }

    public Scene scene() {
        return scene;
    }
    
    public int getVersion() {
        return VERSION;
    }

    public boolean debugging() {
        return DEBUGGING;
    }
    
    public String getLog() {
        return log;
    }

    // METHODS

    /**
     * Print message to console only if debugging, but also logs it
     * 
     * @param message
     * 	The message to print
     */
    public void trace(String message) {
        if(DEBUGGING) {
            System.out.println(message);
        }
        
        log(message);
    }
    
    public void traceEx(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        String stackTrace = sw.toString();
        
        if(DEBUGGING) {
            System.out.println(stackTrace);
        }
        
        log(stackTrace);
    }
    
    /**
     * Send data to log
     * 
     * @param message 
     *  The message to log
     */
    public void log(String message) {
        log += "\n" + message;
    }

    /**
     * 	Returns the current System time as a String
     * 
     * @return
     * 	A String formatted with HH:mm:ss:SS
     */
    public String systemTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SS");
        return sdf.format(cal.getTime());
    }
    
    public String getSystemInfo() {
        return "Operating System: " + System.getProperty("os.name")
                + "\nOS Version: " + System.getProperty("os.version")
                + "\nOS Arch: " + System.getProperty("os.arch")
                + "\nJava Version: " + System.getProperty("java.version")
                + "\nJVM Name: " + System.getProperty("java.vm.name")
                + "\nJVM Version: " + System.getProperty("java.vm.version")
                + "\nSystem Time: " + systemTime();
    }
    
    public static void processOpenLink(String link) {
        try {
            if (Desktop.isDesktopSupported()) {
                // Windows
                Desktop.getDesktop().browse(new URI(link));
            } else {
                // Ubuntu
                Runtime runtime = Runtime.getRuntime();
                runtime.exec("/usr/bin/firefox -new-window " + link);
            }
        } catch(URISyntaxException | IOException ex) {
            ex.printStackTrace();
        }
    }

}
