package application;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import data.Movie;
import database.OMDbBuilder;
import localfiles.FileManager;
import localfiles.FileScanner;

/**
 * The <code>MovieOrganizer</code> runs the main menu loop for the program of the same name.
 * 
 * 	Note: Due to a change in FileManager, there are 3 saveData() sections of the code commented out.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Jul 29, 2016
 *
 */

public class MovieOrganizer {
	// ATTRIBUTES
	public static boolean debugging = false;
	
	public static String saveDataPath = "E:\\MovieOrganizer\\moviesdata_json.txt";
	public static String moviesDirPath = "E:\\1. Movies";
	
	public static ArrayList<Movie> moviesArray = new ArrayList<Movie>();
	private static FileManager fileManager = new FileManager();
	
	public static void main(String[] args) {
		
		// MENU VARIABLES
		Scanner input = new Scanner(System.in);
		boolean printMenu = true;
		
		// ATTEMPT TO LOAD MOVIES FROM SAVEDATA
		/*
		try {
			moviesArray = fileManager.loadData(saveDataPath);			
		} catch(Exception e) {
			System.out.println("\nNo save data found.");
		}
		*/
		
		// CONTINUE TO LOOP MENU UNTIL BROKEN BY 'EXIT' COMMAND
		while(true) {
			if(printMenu) {
				
				// MAIN MENU
				System.out.println("\n\n1. Search Movies\n2. Scan for New Movies\n3. Check for Errors"
						+ "\n4. Update All Movies\n5. Change Movie Directory\n6. Exit\n");
			}
			
			printMenu = true;
			
			// REQUEST USER INPUT
			System.out.print("Input: ");
			String userInput = input.nextLine();
			
			System.out.println();
			
			// GO THROUGH POSSIBLE INPUTS
			if(userInput.equals("1")) {													// SEARCH MOVIES
				
				if(moviesArray.isEmpty()) {
					System.out.println("ERROR: Please scan for movie data first.");
					continue;
				}
				
				System.out.print("Enter search terms: ");
				userInput = input.nextLine();
				
				@SuppressWarnings("unused")
				ArrayList<Movie> searchResults = searchMovies(userInput);
				
			} else if(userInput.equals("2")) {											// SCAN FOR NEW MOVIES
				
				// PERFORM PARTIAL SCAN
				scanFiles(false);
				/*
				try {
					// SAVE THE NEW DATA
					fileManager.saveData(moviesArray, saveDataPath);
				} catch(IOException e) {
					e.printStackTrace();
				}
				*/
				
			} else if(userInput.equals("3")) {											// CHECK FOR ERRORS
				
				
				
			} else if(userInput.equals("4")) {											// UPDATE ALL MOVIES
				
				// PERFORM FULL SCAN
				scanFiles(true);
				/*
				try {
					// SAVE THE NEW DATA
					fileManager.saveData(moviesArray, saveDataPath);
				} catch(IOException e) {
					e.printStackTrace();
				}
				*/
				
			} else if(userInput.equals("5")) {											// CHANGE MOVIE DIRECTORY
				
				
				
			} else if(userInput.equals("6")) {											// EXIT
				
				break;
				
			} else {
				
				// REQUEST NEW INPUT, DO NOT PRINT MENU
				System.out.println("\nInvalid input.\n");
				printMenu = false;
				
			}
		}
		
		// THIS POINT IS ONLY REACHED IF THE MAIN MENU WHILE LOOP IS BROKEN
		System.out.println("Exiting MovieOrganizer...");
		
		input.close();
	}
	
	// METHODS
	
	public static ArrayList<Movie> searchMovies(String searchQuery) {
		// STORE THE LIST OF RESULTS IN AN ARRAYLIST
		ArrayList<Movie> results = new ArrayList<Movie>();
		
		// SPLIT SEARCH TERMS BY COMMAS AND KEYWORDS
		/*
		 * 	KEYWORDS:
		 * 		,			SPLITS SEARCHES (ESSENTIALLY AN 'OR' KEYWORD)
		 * 		and			MUST CONTAIN BOTH TERMS
		 */
		String[] searchTerms = searchQuery.split(", ");
		
		// RIGHT HERE IS WHERE THE DOOBIES GO
		
		int count = 0;
		
		for(Movie movie : moviesArray) {
			for(String term : searchTerms) {
				// CHECK FOR "AND" TERMS
				if(term.contains("and")) {
					String[] andTerms = term.split(" and ");
					
					boolean valid = true;
					
					for(String andTerm : andTerms) {
						if(!movie.containsPhrase(andTerm)) {
							valid = false;
							break;
						}
					}
					
					if(valid) {
						System.out.printf("\n  %4d. " + movie.toTableString(), ++count);
						results.add(movie);
						break;
					}
				} else if(movie.containsPhrase(term)) {
					System.out.printf("\n  %4d. " + movie.toTableString(), ++count);
					results.add(movie);
					break;
				}
			}
		}
		
		return results;
	}
	
	public static void scanFiles(boolean fullScan) {
		// KEEPS TRACK OF NUMBER OF SEARCH ERRORS
		int databaseErrors = 0;
		
		// SCAN FOR ALL MOVIE FILENAMES
		String[] filenames = FileScanner.findFilenames(new File(moviesDirPath));
		
		int fileIndex = 0, dataFileIndex = 0;
		
		// PARSE EACH INDIVIDUAL FILENAME AND SEARCH FOR DATA
		while(fileIndex < filenames.length) {
			
			// IF PERFORMING PARTIAL SCAN AND INSIDE BOUNDS OF DATA
			if(!fullScan && dataFileIndex < moviesArray.size()) {
				/*
				 * 	Looks at alphabetical ordering to determine which to increment
				 * 		
				 * 		If the filename matches the data filename, increment both and continue (skip search)
				 * 		If the filename isn't in the movies ArrayList, perform search
				 * 		If the data filename isn't in the movies folder, delete data
				 * 		Continue to next filename
				 */
				
				if(filenames[fileIndex].equals(moviesArray.get(dataFileIndex).getFilename())) {
					
					// FILENAMES MATCH, INCREMENT BOTH AND JUMP TO TOP OF LOOP
					fileIndex++;
					dataFileIndex++;
					
					continue;
				} else if(filenames[fileIndex].compareTo(moviesArray.get(dataFileIndex).getFilename()) == 1) {
					
					// FILENAME ISN'T IN MOVIES FOLDER ANYMORE, DELETE DATA
					dataFileIndex++;
					
					continue;
				}
				
				// OTHERWISE ALLOW CODE TO CONTINUE TO SEARCH PORTION BELOW
			}
			
			boolean success = checkFilename(filenames[fileIndex]);
			
			if(!success) {
				databaseErrors++;
			}
			
			// ALWAYS INCREMENT FILEINDEX AFTER SEARCHING
			fileIndex++;
		}
		
		// PRINT THE NUMBER OF ERRORS
		System.out.println("\nDatabase Errors: " + databaseErrors);
	}
	
	public static boolean checkFilename(String filename) {
		String[] splitFilename = splitYearFromFilename(filename);
		String response = "";
		
		try {
			// SEARCH AND REFINE SEARCH IN ATTEMPT TO FIND DATA
			response = searchDatabaseForFile(splitFilename[0], splitFilename[1]);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		// EITHER GIVE UP SEARCHING OR WRITE TO THE SAVE FILE
		if(!foundResult(response)) {
			System.out.println("\tERROR: Could not find data for '" + filename + "'");
			return false;
		} else {
			// PRINT CONFIRMATION, AND SEARCH BY IMDB ID
			System.out.println("Found data for '" + filename + "'");
			
			// RETRIEVE IMDB ID FROM FIRST SEARCH RESULT
			String imdbID = getIMDBID(response);

			// GET MOVIE DATA FROM ID
			OMDbBuilder omdb = new OMDbBuilder("", "");
			omdb.setID(imdbID);
			
			// PULL DATA BY ID
			try {
				response = omdb.byID();
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			// LOAD RESPONSE DATA INTO A NEW MOVIE OBJECT
			Movie newMovie = new Movie();
			
			newMovie.fromMovieJSON(fileManager.loadJSONString(response), false);
			
			// ADD FILENAME DATA TO THE MOVIE
			newMovie.setFilename(filename);;
			
			// ADD MOVIE TO THE ARRAYLIST
			moviesArray.add(newMovie);
			
			// REPORT BACK THAT IT WAS A SUCCESS
			return true;
		}
	}
	
	public static String searchDatabaseForFile(String filename, String year) throws Exception {
		
		// PERFORM INITIAL DEFAULT SEARCH
		OMDbBuilder omdb = new OMDbBuilder(filename, year);
		String response = omdb.search();
		
		int seriesEnd = -1, titleStart = -1;
		
		// REFINE SEARCH: REMOVE STANDALONE NUMBERS
		if(!foundResult(response)) {
			
			debugPrint("\n\tInitial search failed, refining search parameters...");
			
			// SEARCH FOR AND REMOVE ALL STANDALONE NUMBERS
			String[] temp = filename.split(" ");
			String newFilename = temp[0];
			
			boolean foundText = !isDigits(temp[0]);
			
			for(int i = 1; i < temp.length; i++) {
				if(!isDigits(temp[i])) {
					foundText = true;
					newFilename += " " + temp[i];
				} else if(!foundText) {
					newFilename += " " + temp[i];
				} else if(seriesEnd == -1) {
					// GRAB THE END INDEX OF THE SERIES AND START INDEX OF THE TITLE
					seriesEnd = newFilename.length();
					titleStart = newFilename.length() + temp[i].length() + 2;
					
					if(titleStart > filename.length()) {
						titleStart = -1;
					}
				}
			}
			
			// ONLY SEARCH IF THE NEW FILENAME IS DIFFERENT THAN THE ORIGINAL
			if(!filename.equals(newFilename)) {
				omdb.setTitle(newFilename);
				
				debugPrint("\t\tAttempting search for '" + newFilename + "'");
				
				response = omdb.search();
			}
		}
		
		// REFINE SEARCH: REMOVE "-" AND REPLACE "&" WITH "AND"
		if(!foundResult(response)) {
			String newFilename = "";
			
			for(int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				
				if(c == '-') {
					// IGNORE
				} else if(c == '&') {
					// REPLACE
					newFilename += "and";
				} else {
					newFilename += c;
				}
			}
			
			// REPLACE ORIGINAL FILENAME WITH NEW ONE
			filename = newFilename;
			
			// NO SEARCHING; LET ONE OF THE NEXT SEARCHES TRY WITH UPDATED FILENAME INSTEAD
		}
		
		// REFINE SEARCH: LOOK UP SERIES NAME AND YEAR [IF SERIES]
		if(!foundResult(response) && seriesEnd != -1) {
			
			String newFilename = filename.substring(0, seriesEnd);
			
			omdb.setTitle(newFilename);
			
			debugPrint("\t\tAttempting search for '" + newFilename + "' with the year '" + year + "'");
			
			response = omdb.search();
		}
		
		// REFINE SEARCH: LOOK UP MOVIE TITLE AND YEAR [WITHOUT SERIES, IF SERIES]
		if(!foundResult(response) && titleStart != -1) {
			
			String newFilename = filename.substring(titleStart);
			
			omdb.setTitle(newFilename);
			
			debugPrint("\t\tAttempting search for '" + newFilename + "' with the year '" + year + "'");
			
			response = omdb.search();
		}
		
		// REFINE SEARCH: LOOK UP FULL NAME WITHOUT YEAR
		if(!foundResult(response)) {
			
			omdb.setTitle(filename);
			omdb.setYear("");

			debugPrint("\t\tAttempting search for '" + filename + "' without the year");
			
			response = omdb.search();
		}
		
		// FINALLY RETURN THE RESPONSE
		return response;
	}
	
	// HELPER METHODS
	
	private static String[] splitYearFromFilename(String filename) {
		for(int i = filename.length() - 1; i >= 0; i--) {
			if(filename.charAt(i) == '(') {
				String[] output = {filename.substring(0, i - 1), filename.substring(i + 1, i + 5)};
				return output;
			}
		}
		
		return new String[1];
	}
	
	private static boolean foundResult(String message) {
		return message.contains("\"Response\":\"True\"");
	}
	
	private static boolean isDigits(String text) {
		for(int i = 0; i < text.length(); i++) {
			if(text.charAt(i) < '0' || text.charAt(i) > '9') {
				return false;
			}
		}
		
		return true;
	}
	
	private static String getIMDBID(String response) {
		int index = response.indexOf("imdbID") + 9;
		
		for(int i = index; i < response.length(); i++) {
			if(response.charAt(i) == '\"') {
				return response.substring(index, i);
			}
		}
		
		return "";
	}
	
	private static void debugPrint(String message) {
		if(debugging) {
			System.out.println(message);
		}
	}
}
