package localfiles;

import data.Movie;
import data.MovieFilenameComparator;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import strings.StringParser;

/**
 * The <code>FileScanner</code> is 
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Jul 24, 2016
 *
 */

public class FileScanner {

    // METHODS
    public static String[] findFilenames(File root) {
        File[] files = findFiles(root);
        String[] filenames = new String[files.length];

        for(int i = 0; i < files.length; i++) {
            filenames[i] = StringParser.filepathToFilename(files[i].toString());
        }

        return filenames;
    }
    
    public static String[] findFilepaths(File root) {
        File[] files = findFiles(root);
        
        String[] filepaths = new String[files.length];

        for(int i = 0; i < files.length; i++) {
            filepaths[i] = files[i].toString();
        }

        return filepaths;
    }
    
    public static String[] findMovieFilepaths(File root, boolean nested) {
        ArrayList<File> files;
        
        if(nested) {
            files = findMovieFilesNested(root);
        } else {
            files = findMovieFiles(root);
        }
        
        String[] filepaths = new String[files.size()];

        for(int i = 0; i < files.size(); i++) {
            filepaths[i] = files.get(i).toString();
        }

        return filepaths;
    }
	
    public static File[] findDirectories(File root) { 
        return root.listFiles(new FileFilter() {
            public boolean accept(File f) {
                return f.isDirectory();
            }
        });
    }

    public static File[] findFiles(File root) {
        return root.listFiles(new FileFilter() {
            public boolean accept(File f) {
                return f.isFile();
            }
        });
    }
    
    // SPECIAL OPTIMIZED ALGORITHM FOR LOADING IN MOVIES SORTED BY FILENAME
    public static ArrayList<Movie> findMovies(File root, boolean nested) {
        ArrayList<File> files;
        
        if(nested) {
            files = findMovieFilesNested(root);
        } else {
            files = findMovieFiles(root);
        }
        
        ArrayList<Movie> movies = new ArrayList<>();

        for(File file : files) {
            
            Movie movie = new Movie();
            movie.setFilename(StringParser.filepathToFilename(file.toString()));
            movie.setFilepath(file.toString().replace(movie.getFilename(), ""));
            
            movies.add(movie);
        }

        // SORT BY FILENAME
        Collections.sort(movies, new MovieFilenameComparator());
        
        return movies;
    }
    
    public static ArrayList<File> findMovieFiles(File root) {
        File[] files = findFiles(root);
        ArrayList<File> movies = new ArrayList<>();
        
        for(File file : files) {
            if(isMovieFile(file.toString())) {
                movies.add(file);
            }
        }
        
        return movies;
        
    }
    
    public static ArrayList<File> findMovieFilesNested(File root) {
        ArrayList<File> movies = new ArrayList<>();
        
        for(File file : root.listFiles()) {
            if(file.isDirectory()) {
                movies.addAll(findMovieFilesNested(file));
            } else if(isMovieFile(file.toString())) {
                movies.add(file);
            }
        }
        
        return movies;
    }
    
    // PRIVATE HELPER METHODS
    private static boolean isMovieFile(String filename) {        
        String[] extensions = {"mkv", "flv", "ogv", "avi", "mov", "wmv", "mp4",
                "m4p", "m4v", "mpg", "mpeg", "mp2", "mpe", "m2v", "m4v", "svi"};
        
        // CHECK IF MOVIE IS A SAMPLE
        if(filename.contains("sample")) {
            return false;
        }
        
        for(String ext : extensions) {
            if(filename.endsWith(ext)) {
                return true;
            }
        }
        
        return false;
    }

}
