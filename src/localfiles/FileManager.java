package localfiles;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

import data.DataManager;
import data.Movie;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import settings.SettingsManager;

/**
 * The <code>FileManager</code> handles all import/export of file data (JSON, plain text, etc)
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Jul 30, 2016
 *
 */
public class FileManager {
    // JSON CONSTANTS
    private final String
            JSON_MOVIES     = "Movies",
            JSON_GARBAGE    = "Garbage",
            JSON_ERRORS     = "Errors";
    
    private final String
            JSON_WINDOW_X           = "WindowX",
            JSON_WINDOW_Y           = "WindowY",
            JSON_WINDOW_WIDTH       = "WindowWidth",
            JSON_WINDOW_HEIGHT      = "WindowHeight",
            JSON_WINDOW_MAXIMIZED   = "WindowMaximized",
            JSON_MOVIES_DIRECTORY   = "MoviesDirectory",
            JSON_VIEW_COLUMNS       = "ViewColumns",
            JSON_SCAN_NESTED        = "ScanNested";

    // METHODS

    /**
     * This method saves data in a JSON formatted file from the data 
     * management component.
     * 
     * @param dataManager
     * 	The data component to save from
     * 
     * @param filepath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    public void saveData(DataManager dataManager, String filepath) throws IOException {
        StringWriter sw = new StringWriter();
        
        // SAVE THE ARRAYS IN JASONARRAYS
        JsonArray moviesArray   = buildMoviesArray(dataManager.getMovies(), false);
        JsonArray garbageArray  = buildMoviesArray(dataManager.getGarbage(), false);
        JsonArray errorsArray   = buildMoviesArray(dataManager.getErrors(), true);

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_MOVIES,   moviesArray)
                .add(JSON_GARBAGE,  garbageArray)
                .add(JSON_ERRORS,   errorsArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filepath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filepath);
        pw.write(prettyPrinted);
        pw.close();
    }
    
    private JsonArray buildMoviesArray(ObservableList<Movie> movies, boolean isFile) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        ObservableList<Movie> errors = movies;

        for(Movie movie : movies) {
            if(isFile) {
                builder.add(movie.toFileJSON());
            } else {
                builder.add(movie.toMovieJSON());
            }
        }
        
        return builder.build();
    }
	
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param dataManager
     *  The data component to load into
     * 
     * @param filepath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    public void loadData(DataManager dataManager, String filepath) throws IOException {        
        
        // CREATE JSON OBJECT FROM FILEPATH
        JsonObject jsonObject = loadJSONFile(filepath);
        
        // CREATE JSON ARRAYS
        JsonArray moviesJsonArray = jsonObject.getJsonArray(JSON_MOVIES);
        JsonArray garbageJsonArray = jsonObject.getJsonArray(JSON_GARBAGE);
        JsonArray errorsJsonArray = jsonObject.getJsonArray(JSON_ERRORS);
        
        // LOAD ALL THE ARRAYS
        ObservableList<Movie> movies = FXCollections.observableArrayList();
        loadMoviesArray(moviesJsonArray, movies, false);
        
        ObservableList<Movie> garbage = FXCollections.observableArrayList();
        loadMoviesArray(garbageJsonArray, garbage, false);
        
        ObservableList<Movie> errors = FXCollections.observableArrayList();
        loadMoviesArray(errorsJsonArray, errors, true);
        
        // LOAD DATA
        dataManager.copyMovies(movies);
        dataManager.copyGarbage(garbage);
        dataManager.copyErrors(errors);
    }
    
    private void loadMoviesArray(JsonArray array, ObservableList<Movie> movies, boolean isFile) {
        for(int i = 0; i < array.size(); i++) {
            Movie movie = new Movie();

            if(isFile) {
                movie.fromFileJSON(array.getJsonObject(i));
            } else {
                movie.fromMovieJSON(array.getJsonObject(i), true);
            }

            movies.add(movie);
        }
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    
    // LOAD A JSONOBJECT FROM A STRING
    public JsonObject loadJSONString(String jsonString) {
    	JsonReader jsonReader = Json.createReader(new StringReader(jsonString));
    	JsonObject jso = jsonReader.readObject();
    	jsonReader.close();
    	
    	return jso;
    }
    
    public void saveSettings(SettingsManager settingsManager, String filepath) throws IOException {
        StringWriter sw = new StringWriter();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_WINDOW_X,         settingsManager.getWindowX())
                .add(JSON_WINDOW_Y,         settingsManager.getWindowY())
                .add(JSON_WINDOW_WIDTH,     settingsManager.getWindowWidth())
                .add(JSON_WINDOW_HEIGHT,    settingsManager.getWindowHeight())
                .add(JSON_WINDOW_MAXIMIZED, settingsManager.getWindowMaximized())
                .add(JSON_MOVIES_DIRECTORY, settingsManager.getMoviesPath())
                .add(JSON_VIEW_COLUMNS,     settingsManager.getViewSettings())
                .add(JSON_SCAN_NESTED,      settingsManager.getScanNested())
                .build();



        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filepath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filepath);
        pw.write(prettyPrinted);
        pw.close();
    }
    
    public void loadSettings(SettingsManager settingsManager, String filepath) throws IOException {
        JsonObject jsonObject = loadJSONFile(filepath);
        
        // LOAD EACH SETTING
        settingsManager.setWindowX(jsonObject.getJsonNumber(JSON_WINDOW_X).doubleValue());
        settingsManager.setWindowY(jsonObject.getJsonNumber(JSON_WINDOW_Y).doubleValue());
        settingsManager.setWindowWidth(jsonObject.getJsonNumber(JSON_WINDOW_WIDTH).doubleValue());
        settingsManager.setWindowHeight(jsonObject.getJsonNumber(JSON_WINDOW_HEIGHT).doubleValue());
        settingsManager.setWindowMaximized(jsonObject.getBoolean(JSON_WINDOW_MAXIMIZED));
        settingsManager.setMoviesPath(jsonObject.getString(JSON_MOVIES_DIRECTORY));        
        settingsManager.setViewSettings(jsonObject.getString(JSON_VIEW_COLUMNS));
        settingsManager.setScanNested(jsonObject.getBoolean(JSON_SCAN_NESTED));
    }
}
