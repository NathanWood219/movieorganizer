package css;

/**
 *  Contains constants that either set the CSS manually or reference the style sheet.
 * 
 * @author Nathan
 */
public class StyleConstants {
    public final static int
            CONTROL_PADDING     = 2,
            LABEL_PADDING	= 4;
    
    public final static String
            CSS_ALIGN_CENTER    = "-fx-alignment: CENTER;",
            CSS_ALIGN_LEFT	= "-fx-alignment: CENTER-LEFT;",
            CSS_ALIGN_RIGHT	= "-fx-alignment: CENTER-RIGHT;",
            CSS_BOLD		= "-fx-font-weight:bold;",
            CSS_FONT_SIZE	= "-fx-font-size:",                                                     // ADD A NUMBER AFTER FOR SIZE
            CSS_PANE_BORDER     = "-fx-border-style: solid inside; -fx-border-width: 1; ";
    
    public final static String
            ROOT            = "root",
            WINDOW          = "window",
            LABEL           = "label",
            BUTTON          = "button",
            TABLE           = "table-view";
}
