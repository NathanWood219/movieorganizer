package tasks;

import application.MainApplication;
import java.util.Properties;
import javafx.concurrent.Task;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Nathan
 */
public class ReportTask extends Task {
    // ATTRIBUTES
    MainApplication app;
    
    String userReport;
    boolean includeSystemInfo;
    
    public ReportTask(MainApplication app, String userReport, boolean includeSystemInfo) {
        this.app = app;
        this.userReport = userReport;
        this.includeSystemInfo = includeSystemInfo;
    }
    
    @Override
    public Integer call() {
        String bugReport = "";
        String line = "--------------------------------------------------";
        
        if(includeSystemInfo) {
            bugReport += line + "\nSYSTEM INFO\n" + line + "\n\n" + app.getSystemInfo() + "\n\n";
        }
        
        if(!userReport.isEmpty()) {
            bugReport += line + "\nUSER REPORT\n" + line + "\n\n" + userReport + "\n\n";
        }
        
        bugReport += line + "\nCONSOLE LOG\n" + line + app.getLog();
        
        try {            
            sendEmail("Bug Report", bugReport);
            app.trace("Successfully sent bug report.");
            
        } catch(Exception e) {
            app.trace("ERROR: Failed to send bug report");
        }
        
        return 0;
    }
    
    public void sendEmail(String subject, String body) throws Exception {
        final String username = "movieorganizerreport@gmail.com";
        final String password = "ByteAddressException";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username));
        message.setRecipients(Message.RecipientType.TO,
            InternetAddress.parse(username));
        message.setSubject(subject);
        message.setText(body);

        Transport.send(message);
    }
    
}
