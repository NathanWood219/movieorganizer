package tasks;

import java.io.File;
import application.MainApplication;
import controllers.WorkspaceController;
import data.DataManager;
import data.Movie;
import database.OMDbBuilder;
import java.util.ArrayList;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import localfiles.FileManager;
import localfiles.FileScanner;
import strings.StringParser;

/**
 * The <code>ScanTask</code> thread allows the JavaFX GUI to continue running
 * 	while scans are processed in the background of the program.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 2, 2016
 *
 */

@SuppressWarnings("rawtypes")
public class ScanTask extends Task {
    // ATTRIBUTES
    MainApplication app;
    DataManager dataManager;
    
    boolean scanNestedFolders;
    
    ObservableList<Movie> errors;
    ObservableList<Movie> garbage;

    DoubleProperty percentageDone;
    
    int errorCount, newMovieCount;

    // CONSTRUCTORS
    public ScanTask(MainApplication app, boolean scanNestedFolders) {
        this.app = app;
        this.dataManager = app.getDataComponent();
        
        this.scanNestedFolders = scanNestedFolders;

        this.errors = FXCollections.observableArrayList();
        this.garbage = FXCollections.observableArrayList();
        
        this.percentageDone = new SimpleDoubleProperty();
        
        errorCount = 0;
        newMovieCount = 0;
    }

    // METHODS
    @Override
    public Integer call() {
        // GRAB MOVIES ARRAY
        ObservableList<Movie> moviesArray = dataManager.getMovies();
        ObservableList<Movie> newMovies = dataManager.getNewMovies();
        
        newMovies.clear();

        // KEEPS TRACK OF NUMBER OF SEARCH ERRORS
        int databaseErrors = 0;

        // SCAN FOR ALL MOVIE FILENAMES
        ArrayList<Movie> scannedMovies = FileScanner.findMovies(new File(app.getSettingsComponent().getMoviesPath()), scanNestedFolders);
        
        app.trace("FileScanner found " + scannedMovies.size() + " movie files.");

        // RESET FILE INDEX
        Integer fileIndex = 0;

        int dataFileIndex = 0;

        // PARSE EACH INDIVIDUAL FILENAME AND SEARCH FOR DATA
        while(fileIndex < scannedMovies.size()) {

            Movie currentMovie = scannedMovies.get(fileIndex);
            
            // SET THE DOUBLE PROPERTY PERCENTAGEDONE TO UPDATE THE LOADING BAR
            percentageDone.set(fileIndex * 1.0 / scannedMovies.size());
            
            // GET FILENAME AND FILEPATH
            String filepath = currentMovie.getFilepath();
            String filename = currentMovie.getFilename();
            
            // IF INSIDE BOUNDS OF DATA
            if(dataFileIndex < moviesArray.size()) {
                /*
                 * 	Looks at alphabetical ordering to determine which to increment
                 * 		
                 * 		If the filename matches the data filename, increment both and continue (skip search)
                 * 		If the filename isn't in the movies ArrayList, perform search
                 * 		If the data filename isn't in the movies folder, delete data
                 * 		Continue to next filename
                 */

                if(filename.equals(moviesArray.get(dataFileIndex).getFilename())) {

                    // FILENAMES MATCH, INCREMENT BOTH AND JUMP TO TOP OF LOOP
                    fileIndex++;
                    dataFileIndex++;

                    continue;
                } else if(filename.compareTo(moviesArray.get(dataFileIndex).getFilename()) == 1) {

                    // ADD TO GARBAGE COLLECTOR TO BE REMOVED AFTER
                    garbage.add(moviesArray.get(dataFileIndex));
                    
                    dataFileIndex++;

                    continue;
                }

                // OTHERWISE ALLOW CODE TO CONTINUE TO SEARCH PORTION BELOW
            }

            // LASTLY, SKIP IF ALREADY IN ERRORS LIST OR GARBAGE LIST
            if(dataManager.hasError(currentMovie) || dataManager.hasGarbage(currentMovie)) {
                fileIndex++;
                continue;
            }
            
            // SEARCH FOR THE FILENAME IN THE DATABASE
            boolean success = checkFile(currentMovie);

            if(!success) {
                errorCount++;
                
                // ADD TO ERRORS LIST                
                if(!dataManager.hasError(currentMovie)) {
                    
                    // ADD TITLE AND YEAR BEFORE PLACING IN DATA MANAGER
                    String[] data = splitYearFromFilename(fixFilename(
                            StringParser.removeExtension(currentMovie.getFilename())));
                    
                    currentMovie.setTitle(data[0]);
                    
                    if(data.length == 1) {
                        currentMovie.setYear("");
                    } else {
                        currentMovie.setYear(data[1]);
                    }
                    
                    dataManager.addError(currentMovie);
                }
            } else {
                newMovies.add(currentMovie);
                newMovieCount++;
            }

            // ALWAYS INCREMENT FILEINDEX AFTER SEARCHING
            fileIndex++;
        }

        // PRINT THE NUMBER OF ERRORS
        app.trace("\nDatabase Errors: " + errorCount);
        app.trace("New Movies: " + newMovieCount);
        
        app.trace("\nRemoving extra movies from garbage...");
        
        // REMOVE GARBAGE MOVIES (DUPLICATES AND EXTRAS)
        for(Movie movie : garbage) {
            moviesArray.remove(movie);
            app.trace("\tRemoved: '" + movie.getFilename() + "'");
        }

        return fileIndex;
    }
    
    // ACCESSORS
    
    public DoubleProperty getPercentageDone() {
        return percentageDone;
    }
    
    public ObservableList<Movie> getErrors() {
        return errors;
    }
    
    public int getErrorCount() {
        return errorCount;
    }
    
    public int getNewMovieCount() {
        return newMovieCount;
    }

    // METHODS
    
    public boolean checkFile(Movie movie) {
        String filepath = movie.getFilepath();
        String filename = movie.getFilename();
        
        // REMOVE PERIODS
        filename = fixFilename(filename);

        // SPLIT THE TITLE FROM THE YEAR
        String[] splitFilename = splitYearFromFilename(filename);

        String response = "";

        try {
            // SEARCH AND REFINE SEARCH IN ATTEMPT TO FIND DATA

            // FIRST CONFIRM THAT THE TITLE HAD THE YEAR THOUGH
            if(splitFilename.length != 2) {
                app.trace("ERROR: The file '" + filename + "' is missing the year.");

                response = searchDatabaseForFile(filename, "");
            } else {
                response = searchDatabaseForFile(splitFilename[0], splitFilename[1]);
            }
        } catch(Exception e) {
            app.traceEx(e);
        }

        // EITHER GIVE UP SEARCHING OR WRITE TO THE SAVE FILE
        if(!foundResult(response)) {
            app.trace("\tERROR: Could not find data for '" + movie.getFilename() + "'");
            
            return false;
        } else {

            // RETRIEVE IMDB ID FROM FIRST SEARCH RESULT
            String imdbID = getJSONValue(response, "imdbID");

            // GET MOVIE DATA FROM ID
            OMDbBuilder omdb = new OMDbBuilder("", "");
            omdb.setID(imdbID);

            // PULL DATA BY ID
            try {
                response = omdb.byID();
            } catch(Exception e) {
                app.traceEx(e);
            }
            
            // CONFIRM SEARCH: IF FOUND, MAKE SURE IT'S A REAL MOVIE
            
            // STORE RUNTIME STRING
            String runtime = getJSONValue(response, "Runtime");

            // IF RUNTIME IS N/A, OR RATING IS N/A AND RUNTIME IS < 60 MIN ---> INVALIDATE RESPONSE
            if(runtime.equals("N/A") || (getJSONValue(response, "Rated").equals("N/A") 
                    && StringParser.getRuntime(runtime) < 60)) {
                
                app.trace("\tERROR: Data found for '" + movie.getFilename() + "' was invalid.");
                
                return false;
                
            } else {
            
                // PRINT CONFIRMATION, AND SEARCH BY IMDB ID
                app.trace("Found data for '" + movie.getFilename() + "'");

                // GRAB FILEMANAGER FROM PARENT APP
                FileManager fileManager = app.getFileComponent();

                movie.fromMovieJSON(fileManager.loadJSONString(response), false);

                // ADD MOVIE TO THE ARRAYLIST
                dataManager.addMovie(movie);

                WorkspaceController workCont = new WorkspaceController(app);
                workCont.processDownloadPoster(movie, false);
            }
            
            // REPORT BACK THAT IT WAS A SUCCESS
            return true;
        }
    }

    public String searchDatabaseForFile(String filename, String year) throws Exception {

        // PERFORM INITIAL DEFAULT SEARCH
        OMDbBuilder omdb = new OMDbBuilder(filename, year);
        String response = omdb.search();

        int seriesEnd = -1, titleStart = -1;

        // REFINE SEARCH: REMOVE STANDALONE NUMBERS
        if(!foundResult(response)) {

            app.trace("\n\tInitial search failed, refining search parameters...");

            // SEARCH FOR AND REMOVE ALL STANDALONE NUMBERS
            String[] temp = filename.split(" ");
            String newFilename = temp[0];

            boolean foundText = !StringParser.isDigits(temp[0]);

            for(int i = 1; i < temp.length; i++) {
                if(!StringParser.isDigits(temp[i])) {
                    foundText = true;
                    newFilename += " " + temp[i];
                } else if(!foundText) {
                    newFilename += " " + temp[i];
                } else if(seriesEnd == -1) {
                    // GRAB THE END INDEX OF THE SERIES AND START INDEX OF THE TITLE
                    seriesEnd = newFilename.length();
                    titleStart = newFilename.length() + temp[i].length() + 2;

                    if(titleStart > filename.length()) {
                        titleStart = -1;
                    }
                }
            }

            // ONLY SEARCH IF THE NEW FILENAME IS DIFFERENT THAN THE ORIGINAL
            if(!filename.equals(newFilename)) {
                omdb.setTitle(newFilename);

                app.trace("\t\tAttempting search for '" + newFilename + "'");

                response = omdb.search();
            }
        }

        // REFINE SEARCH: LOOK UP MOVIE TITLE AND YEAR [WITHOUT SERIES, IF SERIES]
        if(!foundResult(response) && titleStart != -1) {

            String newFilename = filename.substring(titleStart);

            omdb.setTitle(newFilename);

            app.trace("\t\tAttempting search for '" + newFilename + "' with the year '" + year + "'");

            response = omdb.search();
        }

        // REFINE SEARCH: LOOK UP SERIES NAME AND YEAR [IF SERIES]
        if(!foundResult(response) && seriesEnd != -1) {

            String newFilename = filename.substring(0, seriesEnd);

            omdb.setTitle(newFilename);

            app.trace("\t\tAttempting search for '" + newFilename + "' with the year '" + year + "'");

            response = omdb.search();
        }

        // REFINE SEARCH: LOOK UP FULL NAME WITHOUT YEAR
        if(!foundResult(response)) {

            omdb.setTitle(filename);
            omdb.setYear("");

            app.trace("\t\tAttempting search for '" + filename + "' without the year");

            response = omdb.search();
        }

        // FINALLY RETURN THE RESPONSE
        return response;
    }
    
    // HELPER METHODS

    private String fixFilename(String filename) {
        String newFilename = "";

        for(int i = 0; i < filename.length(); i++) {
            char c = filename.charAt(i);

            if(c == '(' || c == ')') {
                // IGNORE
            } else if(c == '&') {
                // REPLACE
                newFilename += "and";
            } else if(c == '.') {
                newFilename += " ";
            } else if(c == ' ') {
                if((i > 0 && filename.charAt(i - 1) == ' ')
                        || (i < filename.length() && filename.charAt(i + 1) == ' ')) {
                    // IGNORE
                } else {
                    newFilename += ' ';
                }
            } else {
                newFilename += c;
            }
        }

        // RETURN NEW FILENAME
        return newFilename;
    }

    private String[] splitYearFromFilename(String filename) {
        if(filename.length() < 4) {
            String[] output = {filename};
            return output;
        }

        for(int i = 0; i < filename.length() - 3; i++) {
            if(StringParser.isDigits(filename.substring(i, i + 4))) {
                String[] output = {filename.substring(0, i - 1), filename.substring(i, i + 4)};
                return output;
            }
        }

        String[] output = {filename};
        return output;
    }

    private boolean foundResult(String message) {
        return message.contains("\"Response\":\"True\"");
    }

    private String getJSONValue(String response, String key) {
        int index = response.indexOf(key) + key.length() + 3;

        for(int i = index; i < response.length(); i++) {
            if(response.charAt(i) == '\"') {
                return response.substring(index, i);
            }
        }

        return "";
    }
}
