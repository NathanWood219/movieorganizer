package tasks;

import application.MainApplication;
import data.Movie;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;

/**
 * The <code>SearchTask</code> thread allows the JavaFX GUI to continue running
 * 	while the search runs in the background.
 * 
 * @author Nathan Wood
 * @version 1.0
 * <b>Date:</b> Aug 2, 2016
 *
 */

@SuppressWarnings("rawtypes")
public class SearchTask extends Task {
    // ATTRIBUTES
    MainApplication app;

    ObservableList<Movie> moviesArray;
    String searchQuery;

    // CONSTRUCTORS
    public SearchTask(MainApplication app, String searchQuery) {
        this.app = app;
        this.searchQuery = searchQuery;

        this.moviesArray = app.getDataComponent().getMovies();
    }

    // ACCESSORS

    // MUTATORS

    // METHODS
    public Integer call() {		
        // SPLIT SEARCH TERMS BY COMMAS AND KEYWORDS
        /*
         * 	KEYWORDS:
         * 		,			SPLITS SEARCHES (ESSENTIALLY AN 'OR' KEYWORD)
         * 		and			MUST CONTAIN BOTH TERMS
         */
        String[] searchTerms = searchQuery.split(", ");

        // RIGHT HERE IS WHERE THE DOOBIES GO

        int index = 0, count = 0;

        for(index = 0; index < moviesArray.size(); index++) {
            Movie movie = moviesArray.get(index);

            for(String term : searchTerms) {
                // CHECK FOR "AND" TERMS
                if(term.contains("and")) {
                    String[] andTerms = term.split(" and ");

                    boolean valid = true;

                    for(String andTerm : andTerms) {
                        if(!movie.containsPhrase(andTerm)) {
                            valid = false;
                            break;
                        }
                    }

                    if(valid) {
                        System.out.printf("\n  %4d. " + movie.toTableString(), ++count);
                        app.getDataComponent().addResult(movie);
                        break;
                    }
            } else if(movie.containsPhrase(term)) {
                    System.out.printf("\n  %4d. " + movie.toTableString(), ++count);
                    app.getDataComponent().addResult(movie);
                    break;
                }
            }
        }

        // ADD SPACING
        System.out.println();

        return index;
    }
	
}
