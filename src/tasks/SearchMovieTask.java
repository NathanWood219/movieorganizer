package tasks;

import application.MainApplication;
import database.OMDbBuilder;
import javafx.concurrent.Task;

/**
 *
 * @author Nathan
 */
public class SearchMovieTask extends Task {
    // ATTRIBUTES
    MainApplication app;
    
    String title, year, response;
    
    public SearchMovieTask(MainApplication app, String title, String year) {
        this.app = app;
        this.title = title;
        this.year = year;
        
        this.response = "";
    }
    
    public String getResponse() {
        return response;
    }
    
    @Override
    public Integer call() throws Exception {
        OMDbBuilder builder = new OMDbBuilder(title, year);
        
        this.response = builder.search();
        
        return 0;
    }
}
