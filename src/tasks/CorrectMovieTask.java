package tasks;

import application.MainApplication;
import controllers.WorkspaceController;
import data.Movie;
import database.OMDbBuilder;
import database.SearchResult;
import javafx.concurrent.Task;

/**
 *
 * @author Nathan
 */
public class CorrectMovieTask extends Task {
    // ATTRIBUTES
    MainApplication app;
    
    Movie movie;
    SearchResult result;
    
    public CorrectMovieTask(MainApplication app, Movie movie, SearchResult result) {
        this.app = app;
        this.movie = movie;
        this.result = result;
    }
    
    @Override
    public Integer call() throws Exception {
        OMDbBuilder builder = new OMDbBuilder(result.getTitle(), result.getYear());
        builder.setID(result.getIMDBid());

        String response = builder.byID();

        app.trace("Correcting movie: '" + movie.getFilename() + "'");

        movie.fromMovieJSON(app.getFileComponent().loadJSONString(response), false);

        app.trace("New movie title: " + movie.getTitle());

        WorkspaceController workCont = new WorkspaceController(app);
        workCont.processDownloadPoster(movie, true);
        
        return 0;
    }
}
